<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!--Author      : @arboshiki-->
<style>
    #invoice {
        padding: 30px;
    }

    .invoice {
        position: relative;
        background-color: #FFF;
        min-height: 680px;
        padding: 15px
    }

    .invoice header {
        padding: 10px 0;
        margin-bottom: 20px;
        border-bottom: 1px solid #3989c6
    }

    .invoice .company-details {
        text-align: right
    }

    .invoice .company-details .name {
        margin-top: 0;
        margin-bottom: 0
    }

    .invoice .contacts {
        margin-bottom: 20px
    }

    .invoice .invoice-to {
        text-align: left
    }

    .invoice .invoice-to .to {
        margin-top: 0;
        margin-bottom: 0
    }

    .invoice .invoice-details {
        text-align: right
    }

    .invoice .invoice-details .invoice-id {
        margin-top: 0;
        color: #3989c6
    }

    .invoice main {
        padding-bottom: 50px
    }

    .invoice main .thanks {
        margin-top: -100px;
        font-size: 2em;
        margin-bottom: 50px
    }

    .invoice main .notices {
        padding-left: 6px;
        border-left: 6px solid #3989c6
    }

    .invoice main .notices .notice {
        font-size: 1.2em
    }

    .invoice table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 20px
    }

    .invoice table td,
    .invoice table th {
        padding: 15px;
        background: #eee;
        border-bottom: 1px solid #fff
    }

    .invoice table th {
        white-space: nowrap;
        font-weight: 400;
        font-size: 16px
    }

    .invoice table td h3 {
        margin: 0;
        font-weight: 400;
        color: #3989c6;
        font-size: 1.2em
    }

    .invoice table .qty,
    .invoice table .total,
    .invoice table .unit {
        text-align: right;
        font-size: 1.2em
    }

    .invoice table .no {
        color: #fff;
        font-size: 1.6em;
        background: #3989c6
    }

    .invoice table .unit {
        background: #ddd
    }

    .invoice table .total {
        background: #3989c6;
        color: #fff
    }

    .invoice table tbody tr:last-child td {
        border: none
    }

    .invoice table tfoot td {
        background: 0 0;
        border-bottom: none;
        white-space: nowrap;
        text-align: right;
        padding: 10px 20px;
        font-size: 1.2em;
        border-top: 1px solid #aaa
    }

    .invoice table tfoot tr:first-child td {
        border-top: none
    }

    .invoice table tfoot tr:last-child td {
        color: #3989c6;
        font-size: 1.4em;
        border-top: 1px solid #3989c6
    }

    .invoice table tfoot tr td:first-child {
        border: none
    }

    .invoice footer {
        width: 100%;
        text-align: center;
        color: #777;
        border-top: 1px solid #aaa;
        padding: 8px 0
    }

    @media print {
        .invoice {
            font-size: 11px !important;
            overflow: hidden !important
        }

        .invoice footer {
            position: absolute;
            bottom: 10px;
            page-break-after: always
        }

        .invoice>div:last-child {
            page-break-before: always
        }
    }

</style>

<div id="invoice">

    <div class="toolbar hidden-print">
        <div class="text-right">
            <button id="printInvoice" onclick="PrintDiv()" class="btn btn-info"><i class="fa fa-print"></i> Print</button>
        </div>
        <hr>
    </div>
    <div class="invoice overflow-auto" id="divTableDataHolder">
        <div style="min-width: 600px">
            <header>
                <div class="row">
                    <div class="col">
                        <a>
                            <img src="{{ asset('/') }}client/assets/images/logo.png" style="height: 120"
                                data-holder-rendered="true" />
                        </a>
                    </div>
                    <div class="col company-details">
                        <h2 class="name">
                            <a target="_blank" href="#">
                                Vape Fish Dubai
                            </a>
                        </h2>
                        <div>455 Foggy Heights, AZ 85004, US</div>
                        <div>(123) 456-789</div>
                        <div>company@example.com</div>
                    </div>
                </div>
            </header>
            <main>
                <div class="row contacts">
                    <div class="col invoice-to">
                        <div class="text-gray-light">INVOICE TO:</div>
                        <h2 class="to">{{ $invoice->first_name . ' ' . $invoice->last_name }}</h2>
                        <div class="address">{{ $invoice->address }}</div>
                        <div class="email"><a href="#">{{ $invoice->email }}</a></div>
                        <div class="email"><a href="#">{{ $invoice->phone }}</a></div>
                        <div class="email"><a href="#">{{ $invoice->phone_alternative }}</a></div>
                    </div>
                    <div class="col invoice-details">
                        <div class="date">Date of Order: {{ formatedDateForDisplay($invoice->created_at) }}
                        </div>
                    </div>
                </div>
                <table border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th class="text-left">Name</th>
                            <th class="text-right">PRICE</th>
                            <th class="text-right">Quantity</th>
                            <th class="text-right">TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($invoice->invoiceDetails as $key => $inv)
                            <tr>
                                <td class="no">{{ $key + 1 }}</td>
                                <td class="text-left">
                                    <h3>{{ $inv->get_product ? $inv->get_product->name : '' }}</h3>
                                </td>
                                <td class="unit">{{ $inv->price }}</td>
                                <td class="qty">{{ $inv->qty }}</td>
                                <td class="total">AED {{ $inv->sub_total }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td colspan="2">GRAND TOTAL</td>
                            <td>AED {{ $invoice->total }}</td>
                        </tr>
                    </tfoot>
                </table>
                <div class="thanks mt-3">Thank you!</div>
                <div class="notices">
                    <div></div>
                    <div class="notice">
                    </div>
                </div>
            </main>
            <footer>
                Invoice was created on a computer and is valid without the signature and seal.
            </footer>
        </div>
        <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
        <div></div>
    </div>
</div>
<script type="text/javascript">
    function PrintDiv() {
        var divToPrint = document.getElementById('divTableDataHolder');
        var popupWin = window.open('', '_blank', 'width=900,height=900');
        var style = `<style type="text/css" media="print">
                        @media print{
                            @page {
                                size: auto;
                            }
                            #data_table_apply_filter{
                                display: none;
                            }

                            table {
                                width:100%;
                                }
                            table, td, th {
                                border: 1px solid #000 !important;
                                border-collapse:initial;
                                border-spacing: 0 !important;
                                padding: 2px 4px !important;
                            }
                            th {
                                vertical-align: middle;text-align: center;
                                }
                            td {
                                 vertical-align: top;
                                }
                            .table th, .table td {
                                    padding: 0px 5px;
                                    color: black;
                                }
                            .table th {
                                background: white !important;
                                padding: 5px !important;
                                color: black !important;
                                }
                            .table-bordered th,
                            .table-bordered td {
                                border: 1px solid #000 !important;
                                }
                            /*.table th {*/
                            /*    background: #ab3535;*/
                            /*    padding: 5px !important;*/
                            /*    color: white !important;*/
                            /*    }*/

                            .hidden_content {
                                   display:none;
                                }
                        }
                    </style>`;
        //    var style='<style type="text/css">table th, table td {border:1px solid #000;padding:0.5em;}</style>';
        popupWin.document.open();
        popupWin.document.write(
            '<html><head><link  rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}"/>' +
            style + '</head><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
        popupWin.document.close();
    }
</script>
