<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $guarded = [];

    // public function getRouteKeyName()
    // {
    //     return 'url';
    // }
    public function get_category()
    {
        return  $this->belongsTo(Category::class, 'category_id', 'id');
    }
    public function get_subcategory()
    {
        return  $this->belongsTo(Category::class, 'subcategory_id', 'id');
    }
    public function get_brand()
    {
        return  $this->belongsTo(Category::class, 'brand_id', 'id');
    }
    public function get_flavour()
    {
        return  $this->belongsTo(Category::class, 'flavour_id', 'id');
    }

    public function get_product_image()
    {
        return $this->hasMany(ProductImage::class, 'product_id', 'id')->where('status',1);
    }
    public function get_color()
    {
        return $this->hasMany(ProductColor::class, 'product_id', 'id');
    }
}
