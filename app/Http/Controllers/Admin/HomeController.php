<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $countCategory = Category::count();
        $countBrand = Brand::count();
        $countProduct = Product::count();
        return view('admin.layouts.home',compact('countCategory','countBrand','countProduct'));
    }
}
