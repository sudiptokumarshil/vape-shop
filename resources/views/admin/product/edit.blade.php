@extends('admin.master')
@section('titles')
    Product
@endsection
@section('admin_home')
    <div class="white_box mb_30">
        <div class="box_header ">
            <div class="main-title">
                <h3 class="mb-0">Update Product</h3>
            </div>
        </div>
        @include('admin.layouts.message')
        <div class="row">
            <div class="col-1"></div>
            <div class="col-10">
                <form action="{{ route('admin.product.update') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Name</label>
                                <input type="text" required value="{{ $product->name }}" name="name"
                                    class="form-control" />
                                <input type="hidden" value="{{ $product->id }}" name="id" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Category</label>
                                <select name="category_id" required onchange="getSubcategory(this.value)"
                                    class="form-control select_2">
                                    <option value="0">Choose One</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}"
                                            {{ $product->category_id == $category->id ? 'selected' : '' }}>
                                            {{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Sub Category</label>
                                <select name="subcategory_id" id="subCateId" class="form-control select_2">
                                    <option value="0">Choose One</option>
                                    @foreach ($subCategories as $subCategory)
                                        <option value="{{ $subCategory->id }}"
                                            {{ $product->subcategory_id == $subCategory->id ? 'selected' : '' }}>
                                            {{ $subCategory->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Brand</label>
                                <select name="brand_id" id="brandId" class="form-control select_2">
                                    <option value="0">Choose One</option>
                                    @foreach ($brands as $brand)
                                        <option value="{{ $brand->id }}"
                                            {{ $product->brand_id == $brand->id ? 'selected' : '' }}>
                                            {{ $brand->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Flavour</label>
                                <select name="flavour_id" id="flavourId" class="form-control select_2">
                                    <option value="0">Choose One</option>
                                    @foreach ($flavours as $flavour)
                                        <option value="{{ $flavour->id }}"
                                            {{ $product->flavour_id == $flavour->id ? 'selected' : '' }}>
                                            {{ $flavour->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Price</label>
                                <input type="number" required value="{{ $product->price }}" name="price"
                                    class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Discount</label>
                                <input type="number" value="{{ $product->discount }}" name="discount"
                                    class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Stock</label>
                                <input type="number" required value="{{ $product->stock }}" name="stock"
                                    class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Image</label>
                                <input type="file" name="image" id="img" class="form-control" />
                                </br>
                                <img id="blah" src="{{ productImagePath($product->image) }}" alt=""
                                    style="height:100px; border: 2px solid grey" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Short Description</label>
                                <textarea name="short_description" id="short-description" class="form-control">{!! $product->short_description !!}</textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Description</label>
                                <textarea name="description" id="long-description" class="form-control">{!! $product->description !!}</textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Title</label>
                                <input name="title" required id="title" value="{!! $product->title !!}"
                                    class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Meta</label>
                                <input name="meta" required id="meta" value="{!! $product->meta !!}"
                                    class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Url</label>
                                <input name="url" required id="url" value="{!! $product->url !!}"
                                    class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Stock Available </label>
                                <select name="is_stock" class="form-control">
                                    <option value="1" {{ $product->is_stock == 1 ? 'selected' : '' }}>Stock
                                        Available
                                    </option>
                                    <option value="2" {{ $product->is_stock == 2 ? 'selected' : '' }}>Out Of Stock
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Status</label>
                                <select name="status" class="form-control">
                                    <option value="1" {{ $product->status == 1 ? 'selected' : '' }}>Active</option>
                                    <option value="2" {{ $product->status == 2 ? 'selected' : '' }}>Inactive
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="exampleInputEmail1">Colors/Flavours</label>
                            <div class="form-group" id="colors">
                                <div class="row" id="color-option-0">
                                    <div class="col-md-4">
                                        <input type="text"
                                            value="{{ !empty($product->get_color[0]) ? $product->get_color[0]['name'] : '' }}"
                                            required name="color[]" class="form-control" />
                                        <input type="hidden"
                                            value="{{ !empty($product->get_color[0]) ? $product->get_color[0]['id'] : 0 }}"
                                            name="color_id[]" />
                                    </div>
                                    <div class="col-md-4">
                                        <input type="file" name="color_image[]" class="form-control" />
                                        <br>
                                        @if (isset($product->get_color[0]))
                                            <img id="blahs"
                                                src="{{ productImagePath($product->get_color[0]['color_image']) }}"
                                                alt="" style="height:100px; border: 2px solid grey" />
                                        @endif
                                    </div>
                                    <div class="col-md-4">
                                        <div class="btn-group">
                                            <button type="button" onclick="IncColor(0)"
                                                class="btn btn-success btn-sm">+</button>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                @foreach ($product->get_color as $key => $color)
                                    @if ($key != 0)
                                        <div class="row" id="color-option-{{ $key }}">
                                            <div class="col-md-4">
                                                <input type="text" value="{{ $color->name }}" name="color[]"
                                                    class="form-control" />
                                                <input type="hidden" value="{{ $color->id }}" name="color_id[]" />
                                            </div>
                                            <div class="col-md-4">
                                                <input type="file" name="color_image[]" class="form-control" />
                                                <br>
                                                <img id="blahs" src="{{ productImagePath($color->color_image) }}"
                                                    alt="" style="height:100px; border: 2px solid grey" />
                                            </div>
                                            <div class="col-md-4">
                                                <div class="btn-group">
                                                    <button type="button" onclick="IncColor({{ $key }})"
                                                        class="btn btn-success btn-sm">+</button>
                                                    &nbsp;
                                                    <button type="button" onclick="decColor({{ $key }})"
                                                        class="btn btn-danger btn-sm">-</button>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm">Update</button>
                    <a href="{{ route('admin.product.list') }}" class="btn btn-info btn-sm">Go To List</a>
                </form>
            </div>
            <div class="col-1"></div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://cdn.ckeditor.com/ckeditor5/34.0.0/classic/ckeditor.js"></script>
    <script>
        // NOTE: FOR DISPLAYING SELECTED PRODUCT IMAGE ...
        const readURL = (input) => {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#blah').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
            $('#blah').show();
        }
        $("#img").change(function() {
            readURL(this);
        });
        // NOTE: FOR GETTING CATEGORY WISE SUBCATEGORY ...
        const getSubcategory = (id) => {
            $.ajax({
                url: "/admin/category/get-subcategory/" + id,
                type: "GET",
                data: {
                    _token: "{{ csrf_token() }}",
                },
                success: function(data) {
                    $('#subCateId').empty();
                    $('#brandId').empty();
                    $('#flavourId').empty();
                    let dataHtml = "";
                    let dataBHtml = "";
                    let dataFHtml = "";
                    dataHtml += `<option value='0'>Choose One</option>`;
                    $.each(data.subCategory, function(index, item) {
                        dataHtml += `<option value='${item.id}'>${item.name}</option>`;
                    });
                    dataBHtml += `<option value='0'>Choose One</option>`;
                    $.each(data.brands, function(index, item) {
                        dataBHtml += `<option value='${item.id}'>${item.name}</option>`;
                    });
                    dataFHtml += `<option value='0'>Choose One</option>`;
                    $.each(data.flavours, function(index, item) {
                        dataFHtml += `<option value='${item.id}'>${item.name}</option>`;
                    });
                    $('#subCateId').html(dataHtml);
                    $('#brandId').html(dataBHtml);
                    $('#flavourId').html(dataFHtml);
                },
            });
        }
    </script>
    <script>
        ClassicEditor
            .create(document.querySelector('#short-description'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });

        ClassicEditor
            .create(document.querySelector('#long-description'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });

        const IncColor = (id) => {
            id++
            let dataHtml = '';
            dataHtml += `<br>
                    <div class="row" id="color-option-${id}">
                        <div class="col-md-4">
                            <input type="text" required name="color[]" class="form-control" />
                        </div>
                        <div class="col-md-4">
                            <input type="file" required name="color_image[]" class="form-control" />

                        </div>
                        <div class="col-md-4">
                            <div class="btn-group">
                                <button type="button" onclick="IncColor(${id})"
                                    class="btn btn-success btn-sm">+</button>
                                    &nbsp;
                                <button type="button" onclick="decColor(${id})" class="btn btn-danger btn-sm">-</button>
                            </div>
                        </div>
                        <input type="hidden" name="color_id[]"
                    </div>
                `;
            $('#colors').append(dataHtml);

        }

        const decColor = (id) => {
            console.log(id);
            $('#color-option-' + id).remove();

        }
    </script>
@endsection
