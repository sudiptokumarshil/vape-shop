@extends('admin.master')
@section('titles')
    Sub Category
@endsection
@section('admin_home')
    <div class="QA_section">
        <div class="white_box_tittle list_header">
            <h4>Sub Category</h4>
            <div class="box_right d-flex lms_block">
                <div class="serach_field_2">

                </div>
                <div class="add_button ml-10">
                    <a href="{{ route('admin.subcategory.create') }}" class="btn_1">Add
                        New</a>
                    <a href="{{ route('admin.category.list') }}" class="btn_1">Add
                        Category</a>

                </div>
            </div>
        </div>
        @include('admin.layouts.message')
        <div class="QA_table mb_30">
            <table class="table lms_table_active">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        {{-- <th scope="col">Heading</th> --}}
                        <th scope="col">Parent Category</th>
                        <th scope="col">Type</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php($i = 1)
                    @foreach ($categories as $category)
                        <tr>
                            <th scope="row"> {{ $i++ }}</th>
                            <td>{{ $category->name }}</td>
                            {{-- <td>{{ $category->heading }}</td> --}}
                            <td>{{ $category->get_category ? $category->get_category->name : 'Not Found' }}</td>
                            <td>
                                @if ($category->type == 1)
                                    Sucategory
                                @elseif($category->type == 2)
                                    Brand
                                @elseif($category->type == 3)
                                    flovour
                                @endif
                            </td>
                            <td>
                                @if ($category->status == 1)
                                    Active
                                @else
                                    Inactive
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('admin.subcategory.edit', ['id' => $category->id]) }}" type="button"
                                    class="btn btn-warning">Edit</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="d-flex justify-content-center">
                {!! $categories->links() !!}
            </div>
        </div>
    </div>
@endsection
