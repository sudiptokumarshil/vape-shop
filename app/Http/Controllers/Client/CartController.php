<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Traits\CommonTrait;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;

class CartController extends Controller
{
    use CommonTrait;
    public function index()
    {
        $cart = Cart::content();
        $total = Cart::priceTotal();
        $categories = $this->getCategory(); //NOTE: FOR GETTING CATEGORIES ...
        return view('client.layouts.pages.cart', [
            'carts' => $cart,
            'total' => $total,
            'categories' => $categories,
        ]);
    }

    public function store(Request $request)
    {
        $product = Product::where('id', $request->id)->first();
        Cart::add([
            'id' => $request->id,
            'name' => $product->name,
            'qty' => 1,
            'price' => getPriceAfterDiscount($product->price, $product->discount),
            'weight' => 0,
            'options' =>
            [
                'image' => $product->image,
                'category_id' => $product->category_id,
                'url' => $product->url,
            ],
        ]);
        $total = Cart::priceTotal();;
        $cart_list = Cart::content();
        $count = Cart::content()->count();
        $data = [
            "list" => $cart_list,
            "total" => $total,
            "count" => $count,
        ];
        return response()->json($data);
    }
    public function update(Request $request)
    {
        $qty = Cart::get($request->rowId);
        if ($request->status == 1) {
            Cart::update($request->rowId, $qty->qty + 1);
        } elseif ($request->status == 2) {
            ($qty->qty > 1) ? Cart::update($request->rowId, $qty->qty - 1) : '';
        }

        $total = Cart::priceTotal();;
        $cart_list = Cart::content();
        $count = Cart::content()->count();

        $data = [
            "list" => $cart_list,
            "total" => $total,
            "count" => $count,
            "qty" => $qty,
        ];
        return response()->json($data);
    }

    public function delete(Request $request)
    {
        Cart::remove($request->rowId);
        $total = Cart::priceTotal();;
        $cart_list = Cart::content();
        $count = Cart::content()->count();
        $data = [
            "list" => $cart_list,
            "total" => $total,
            "count" => $count,
        ];
        return response()->json($data);
    }

    public function checkCartIsEmpty()
    {
        $count = Cart::content()->count();
        return response()->json($count);
    }
}
