@extends('client.master')
@section('client_title')
 Shipping Information
@endsection
@section('home')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumb-trail breadcrumbs">
                <ul class="trail-items breadcrumb">
                    <li class="trail-item trail-begin">
                        <a href="index.html">Home</a>
                    </li>
                    <li class="trail-item trail-end active">
                        Shipping Policy
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="content-area content-contact col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="site-main">
                <h3 class="custom_blog_title">Shipping Policy</h3>
            </div>
        </div>
    </div>
</div>
<div class="page-main-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="main-info-post">
                    <p>
                        Customers can avail themselves of nationwide and worldwide shipping options when ordering online. We process one shipping address per order. If you would like to ship to a different address, you may consider placing separate orders and making payments for them separately. The shipping date depends on your preferred delivery location and delivery type.
                    </p>
                    <p style="font-size: 20px; margin-bottom: 130px !important">
                        <b>FREE SHIPPING ABOVE 500 AED SHOPPING!</b>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
