@extends('client.master')
@section('client_title')
    Vape Fish Dubai
@endsection
@section('home')
    @include('client.layouts.slider')
    <div class="herald-iconbox-wrapp rows-space-10">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 herald-iconbox-wrapp-small">
                    <div class="herald-iconbox default">
                        <div class="iconbox-inner">
                            <div class="icon">
                                <span class="fa fa-truck"></span>
                            </div>
                            <div class="content">
                                <h4 class="title">
                                    Free Delivery
                                </h4>
                                <div class="text">
                                    On order over AED500
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 herald-iconbox-wrapp-small">
                    <div class="herald-iconbox default">
                        <div class="iconbox-inner">
                            <div class="icon">
                                <span class="fa fa-shield"></span>
                            </div>
                            <div class="content">
                                <h4 class="title">
                                    Money Guarantee
                                </h4>
                                <div class="text">
                                    30 Days money back!
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 herald-iconbox-wrapp-small last-item">
                    <div class="herald-iconbox default">
                        <div class="iconbox-inner">
                            <div class="icon">
                                <span class="fa fa-life-ring"></span>
                            </div>
                            <div class="content">
                                <h4 class="title">
                                    Online Support 24/7
                                </h4>
                                <div class="text">
                                    We’re Always here!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- <div class="banner-video-wrapp rows-space-40 type2">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="banner">
                        <div class="item-banner style9">
                            <div class="inner">
                                <div class="banner-content">
                                    <h4 class="herald-subtitle">Hurry up</h4>
                                    <h3 class="title">Big Sale Up<br /> To 30% Off</h3>
                                    <div class="code">
                                        Use promo Code:
                                        <span class="nummer-code">HERALD</span>
                                    </div>
                                    <a href="#" class="button btn-shop-now">Shop now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="banner">
                        <div class="item-banner style9 type1">
                            <div class="inner">
                                <div class="banner-content">
                                    <h4 class="herald-subtitle">Sale Up to 50% Off</h4>
                                    <h3 class="title"> New Good On <br /> The Shop</h3>
                                    <div class="code">
                                        Use promo Code:
                                        <span class="nummer-code">HERALD</span>
                                    </div>
                                    <a href="#" class="button btn-shop-now">Shop now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <div class="herald-product produc-featured rows-space-40">
        <div class="container">
            <div class="custommenu-title-blog-wrap">
                <h3 class="custommenu-title-blog">
                    New Arrivals
                </h3>
                <a href="#" class="view-all">View All</a>
            </div>
            <ul class="row list-products auto-clear equal-container product-grid">
                @foreach ($products as $product)
                    <li class="product-item  col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
                        <div class="product-inner equal-element">
                            <div class="product-top">
                                {{-- <div class="flash">
                                    <span class="onnew">
                                        <span class="text">
                                            new
                                        </span>
                                    </span>
                                </div> --}}
                                {!! displayStockAvailable($product->is_stock) !!}
                            </div>
                            <div class="product-thumb">
                                <div class="thumb-inner">
                                    <a href="{{ route('client.product.details', ['slug' => $product->url]) }}">
                                        <img src="{{ productImagePath($product->image) }}" alt="img">
                                    </a>
                                    <div class="thumb-group">
                                        {{-- <div class="yith-wcwl-add-to-wishlist">
                                            <div class="yith-wcwl-add-button">
                                                <a href="#">Add to Wishlist</a>
                                            </div>
                                        </div>
                                        <a href="#" class="button quick-wiew-button">Quick View</a> --}}
                                        <div class="loop-form-add-to-cart">
                                            <button id="cartbtn-{{ $product->id }}"
                                                class="single_add_to_cart_button button"
                                                onclick="addToCart({{ $product->id }})">Add to cart
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-info">
                                <h5 class="product-name product_title">
                                    <a
                                        href="{{ route('client.product.details', ['slug' => $product->url]) }}">{{ $product->name }}</a>
                                </h5>
                                <div class="group-info">
                                    <div class="price">
                                        @if (!empty($product->discount))
                                            <del>
                                                AED {{ $product->price }}
                                            </del>
                                            <ins>
                                                AED {{ getPriceAfterDiscount($product->price, $product->discount) }}
                                            </ins>
                                        @else
                                            <ins>
                                                AED {{ $product->price }}
                                            </ins>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
            @if (!empty($products[0]))
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div style="width: fit-content; margin: 0 auto">
                        <a href="{{ route('client.product.get_new_product') }}" class="">
                            <button>
                                View All
                            </button>
                        </a>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div class="herald-product produc-featured rows-space-40">
        <div class="container">
            <div class="custommenu-title-blog-wrap">
                <h3 class="custommenu-title-blog">
                    Disposable
                </h3>
                <a href="{{ route('client.product.category_product', ['id' => 11,'type'=>1]) }}" class="view-all">View
                    All</a>
            </div>
            <ul class="row list-products auto-clear equal-container product-grid">
                @foreach ($disposable as $product)
                    <li class="product-item  col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
                        <div class="product-inner equal-element">
                            <div class="product-top">
                                {{-- <div class="flash">
                                    <span class="onnew">
                                        <span class="text">
                                            new
                                        </span>
                                    </span>
                                </div> --}}
                                {!! displayStockAvailable($product->is_stock) !!}
                            </div>
                            <div class="product-thumb">
                                <div class="thumb-inner">
                                    <a href="{{ route('client.product.details', ['slug' => $product->url]) }}">
                                        <img src="{{ productImagePath($product->image) }}" alt="img">
                                    </a>
                                    <div class="thumb-group">
                                        {{-- <div class="yith-wcwl-add-to-wishlist">
                                            <div class="yith-wcwl-add-button">
                                                <a href="#">Add to Wishlist</a>
                                            </div>
                                        </div>
                                        <a href="#" class="button quick-wiew-button">Quick View</a> --}}
                                        <div class="loop-form-add-to-cart">
                                            <button id="cartbtn-{{ $product->id }}"
                                                class="single_add_to_cart_button button"
                                                onclick="addToCart({{ $product->id }})">Add to cart
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-info">
                                <h5 class="product-name product_title">
                                    <a
                                        href="{{ route('client.product.details', ['slug' => $product->url]) }}">{{ $product->name }}</a>
                                </h5>
                                <div class="group-info">
                                    <div class="price">
                                        @if (!empty($product->discount))
                                            <del>
                                                AED {{ $product->price }}
                                            </del>
                                            <ins>
                                                AED {{ getPriceAfterDiscount($product->price, $product->discount) }}
                                            </ins>
                                        @else
                                            <ins>
                                                AED {{ $product->price }}
                                            </ins>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
            @if (!empty($disposable[0]))
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div style="width: fit-content; margin: 0 auto">
                        <a href="{{ route('client.product.category_product', ['id' => 11,'type'=>1]) }}" class="">
                            <button>
                                View All
                            </button>
                        </a>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div class="herald-product produc-featured rows-space-40">
        <div class="container">
            <div class="custommenu-title-blog-wrap">
                <h3 class="custommenu-title-blog">
                    Juice
                </h3>
                <a href="{{ route('client.product.category_product', ['id' => 5,'type'=>1]) }}" class="view-all">View All</a>
            </div>
            <ul class="row list-products auto-clear equal-container product-grid">
                @foreach ($juice as $product)
                    <li class="product-item  col-lg-3 col-md-4 col-sm-6 col-xs-6 col-ts-12 style-1">
                        <div class="product-inner equal-element">
                            <div class="product-top">
                                {{-- <div class="flash">
                                    <span class="onnew">
                                        <span class="text">
                                            new
                                        </span>
                                    </span>
                                </div> --}}
                                {!! displayStockAvailable($product->is_stock) !!}
                            </div>
                            <div class="product-thumb">
                                <div class="thumb-inner">
                                    <a href="{{ route('client.product.details', ['slug' => $product->url]) }}">
                                        <img src="{{ productImagePath($product->image) }}" alt="img">
                                    </a>
                                    <div class="thumb-group">
                                        {{-- <div class="yith-wcwl-add-to-wishlist">
                                            <div class="yith-wcwl-add-button">
                                                <a href="#">Add to Wishlist</a>
                                            </div>
                                        </div>
                                        <a href="#" class="button quick-wiew-button">Quick View</a> --}}
                                        <div class="loop-form-add-to-cart">
                                            <button id="cartbtn-{{ $product->id }}"
                                                class="single_add_to_cart_button button"
                                                onclick="addToCart({{ $product->id }})">Add to cart
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-info">
                                <h5 class="product-name product_title">
                                    <a
                                        href="{{ route('client.product.details', ['slug' => $product->url]) }}">{{ $product->name }}</a>
                                </h5>
                                <div class="group-info">
                                    <div class="price">
                                        @if (!empty($product->discount))
                                            <del>
                                                AED {{ $product->price }}
                                            </del>
                                            <ins>
                                                AED {{ getPriceAfterDiscount($product->price, $product->discount) }}
                                            </ins>
                                        @else
                                            <ins>
                                                AED {{ $product->price }}
                                            </ins>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
            @if (!empty($juice[0]))
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div style="width: fit-content; margin: 0 auto">
                        <a href="{{ route('client.product.category_product', ['id' => 5,'type'=>1]) }}" class="">
                            <button>
                                View All
                            </button>
                        </a>
                    </div>
                </div>
            @endif
        </div>
    </div>
    {{-- <div class="banner-pinmap-wrapp rows-space-70">
        <div class="banner">
            <div class="item-banner style21">
                <div class="inner">
                    <div class="banner-content container">
                        <h4 class="herald-subtitle">Style your car!</h4>
                        <h3 class="title">
                            Collection<br />
                            Sale <span>15%</span> Off
                        </h3>
                        <div class="start-from">
                            start from <span>Dec 27</span> to <span>dec 29</span>
                        </div>
                        <a href="#" class="button btn-shop-now">Shop now</a>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    {{-- <div class="herald-testimonials-newsletter-wrapp">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="herald-testimonials-wrapp">
                        <div class="herald-testimonials default">
                            <div class="owl-slick equal-container"
                                data-slick='{"autoplay":false, "autoplaySpeed":1000, "arrows":false, "dots":true, "infinite":true, "speed":800}'
                                data-responsive='[{"breakpoint":"2000","settings":{"slidesToShow":1}}]'>
<div class="testimonial-item">
    <div class="image">
        <img src="assets/images/testimonial-1.jpg" alt="img">
    </div>
    <div class="info">
        <div class="text">
            <p>
                Maecenas vel nulla eleifend, euismod magna sed, tristique velit.
                Nam
                sed eleifend dui, eu eleifend leo. Mauris ornare eros quis
                placerat
                mollis zuis ornare euismod.
            </p>
        </div>
        <h5 class="name">
            Adam Smith
            <span>Shop Owner</span>
        </h5>
    </div>
</div>
<div class="testimonial-item">
    <div class="image">
        <img src="assets/images/testimonial-2.jpg" alt="img">
    </div>
    <div class="info">
        <div class="text">
            <p>
                Maecenas vel nulla eleifend, euismod magna sed, tristique velit.
                Nam
                sed eleifend dui, eu eleifend leo. Mauris ornare eros quis
                placerat
                mollis zuis ornare euismod.
            </p>
        </div>
        <h5 class="name">
            Adam Smith
            <span>Shop Owner</span>
        </h5>
    </div>
</div>
</div>
</div>
</div>
</div>
<div class="col-lg-3">
    <div class="herald-newsletter default">
        <div class="newsletter-head">
            <h3 class="title">Newsletter</h3>
            <div class="subtitle">Get more special Deals, Events & Promotions</div>
        </div>
        <div class="newsletter-form-wrap">
            <input class="input-text email email-newsletter" type="email" name="email" placeholder="Your email here...">
            <button class="button btn-submit submit-newsletter">Submit</button>
        </div>
    </div>
</div>
</div>
</div>
</div> --}}
    {{-- <div class="banner-wrapp rows-space-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="banner">
                        <div class="item-banner style16">
                            <div class="inner">
                                <div class="banner-content">
                                    <h3 class="title">Smoke <br />Fruitys</h3>
                                    <div class="description">
                                        Herald style, day by day <br />functionality!
                                    </div>
                                    <a href="#" class="button btn-view-the-look">View all</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="banner">
                        <div class="item-banner style15">
                            <div class="inner">
                                <div class="banner-content">
                                    <h3 class="title">Cigar <br />Lookbook</h3>
                                    <div class="description">
                                        Men with Cigar <br />Summer Lookbook
                                    </div>
                                    <a href="#" class="button btn-view-the-look">View all</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <div class="herald-product layout1" style="display: none;">
        <div class="container">
            <div class="container-wapper">
                <div class="head">
                    <h3 class="title">Weekly Featured</h3>
                    <div class="subtitle">Let’s Shop our featured item this week</div>
                </div>
                <div class="product-list-owl owl-slick equal-container nav-center-left"
                    data-slick='{"autoplay":false, "autoplaySpeed":1000, "arrows":true, "dots":false, "infinite":true, "speed":800,"infinite":false}'
                    data-responsive='[{"breakpoint":"2000","settings":{"slidesToShow":3}},{"breakpoint":"1200","settings":{"slidesToShow":2}},{"breakpoint":"992","settings":{"slidesToShow":1}},{"breakpoint":"768","settings":{"slidesToShow":2}},{"breakpoint":"481","settings":{"slidesToShow":1}}]'>
                    @foreach ($weeklyProducts as $weeklyProduct)
                        <div class="product-item style-1 product-type-variable">
                            <div class="product-inner equal-element">
                                <div class="product-top">
                                    {{-- <div class="flash">
                                        <span class="onnew">
                                            <span class="text">
                                                new
                                            </span>
                                        </span>
                                    </div> --}}
                                    {!! displayStockAvailable($product->is_stock) !!}
                                </div>
                                <div class="product-thumb">
                                    <div class="thumb-inner">
                                        <a href="{{ route('client.product.details', ['slug' => $product->url]) }}">
                                            <img src="{{ productImagePath($weeklyProduct->image) }}" alt="img" />
                                        </a>
                                        {{-- <div class="thumb-group">
                                            <div class="yith-wcwl-add-to-wishlist">
                                                <div class="yith-wcwl-add-button">
                                                    <a href="#">Add to Wishlist</a>
                                                </div>
                                            </div>
                                            <a href="#" class="button quick-wiew-button">Quick View</a>
                                            <div class="loop-form-add-to-cart">
                                                <button class="single_add_to_cart_button button">Add to cart
                                                </button>
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                                <div class="product-info">
                                    <h5 class="product-name product_title">
                                        <a
                                            href="{{ route('client.product.details', ['slug' => $product->url]) }}">{{ $weeklyProduct->name }}</a>
                                    </h5>
                                    <div class="group-info">
                                        {{-- <div class="stars-rating">
                                            <div class="star-rating">
                                                <span class="star-3"></span>
                                            </div>
                                            <div class="count-star">
                                                (3)
                                            </div>
                                        </div> --}}
                                        <div class="price">
                                            @if (!empty($weeklyProduct->discount))
                                                <del>
                                                    {{ $weeklyProduct->price }}
                                                </del>
                                                <ins>
                                                    {{ getPriceAfterDiscount($weeklyProduct->price, $weeklyProduct->discount) }}
                                                </ins>
                                            @else-
                                                <ins>
                                                    {{ $weeklyProduct->price }}
                                                </ins>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
@section('front_script')
    <script>
        $(document).ready(function() {
            if (typeof localStorage.getItem("client_ip") === 'undefined' || localStorage.getItem("client_ip") ==
                null) {
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger'
                    },
                    buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                    title: 'Welcome',
                    text: "Please verify that you are 21 years of age or older to enter this site.",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes!',
                    cancelButtonText: 'No!',
                    reverseButtons: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        ClientIp();

                    } else if (
                        /* Read more about handling dismissals below */
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        redirect();
                    }
                })
            }
        });

        const redirect = () => {
            return location.href = "https://www.google.com/";
        }

        const ClientIp = () => {
            $.get('https://www.cloudflare.com/cdn-cgi/trace', function(data) {
                data = data.trim().split('\n').reduce(function(obj, pair) {
                    pair = pair.split('=');
                    return obj[pair[0]] = pair[1], obj;
                }, {});
                localStorage.setItem("client_ip", data.ip);

            });
        }
    </script>
@endsection
