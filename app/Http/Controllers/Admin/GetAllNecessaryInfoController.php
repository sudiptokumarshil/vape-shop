<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class GetAllNecessaryInfoController extends Controller
{
    public function getSubcategory($id)
    {
        $subCategory = Category::select('id', 'name', 'status', 'root_id', 'type')
            ->where('root_id', $id)
            ->where('status', 1)
            ->where('type', 1)
            ->get();

        $brands = Category::select('id', 'name', 'status', 'root_id', 'type')
            ->where('root_id', $id)
            ->where('status', 1)
            ->where('type', 2)
            ->get();
            
        $flavours = Category::select('id', 'name', 'status', 'root_id', 'type')
            ->where('root_id', $id)
            ->where('status', 1)
            ->where('type', 3)
            ->get();

        return response()->json([
            'subCategory' => $subCategory,
            'brands' => $brands,
            'flavours' => $flavours,
        ]);
    }
}
