<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function index()
    {
        $brands = Brand::orderBy('id', 'desc')->paginate(10);
        return view('admin.brand.list', [
            'brands' => $brands
        ]);
    }
    public function create()
    {
        return view('admin.brand.create');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required||unique:brands,name',
        ]);
        $brand = new Brand();
        $brand->name = $request->name;
        $brand->status = $request->status;
        $brand->save();
        return redirect()->back()->with('success', 'Brand Saved Successfully!!');
    }
    public function edit($id)
    {
        $brand = Brand::find($id);
        return view('admin.brand.edit', [
            'brand' => $brand
        ]);
    }
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required||unique:brands,name,' . $request->id,
        ]);
        $brand = Brand::find($request->id);
        $brand->name = $request->name;
        $brand->status = $request->status;
        $brand->update();
        return redirect()->back()->with('success', 'Brand Updated Successfully!!');
    }
}
