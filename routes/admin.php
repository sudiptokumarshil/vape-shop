<?php

use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ContactUsMailController;
use App\Http\Controllers\Admin\FilterController;
use App\Http\Controllers\Admin\GetAllNecessaryInfoController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\RealTimeBroadcastController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Admin\SubCategoryController;
use Illuminate\Support\Facades\Route;
use Pusher\Pusher;

// Admin Routes
Route::group(['prefix' => 'admin', 'middleware' => 'admin:admin'], function () {
    Route::get('/dashboard', [HomeController::class, 'index'])->name('admin.home');
    Route::post('/dashboard', [LoginController::class, 'logout'])->name('admin.logout');

    // ============================ FOR CATEGORY =====================================
    Route::get('/category/list', [CategoryController::class, 'index'])->name('admin.category.list');
    Route::get('/category/create', [CategoryController::class, 'create'])->name('admin.category.create');
    Route::post('/category/create', [CategoryController::class, 'store'])->name('admin.category.store');
    Route::get('/category/edit/{id}', [CategoryController::class, 'edit'])->name('admin.category.edit');
    Route::post('/category/update', [CategoryController::class, 'update'])->name('admin.category.update');
    Route::get('/category/get-subcategory/{id}', [GetAllNecessaryInfoController::class, 'getSubcategory']);
    // ============================ FOR CATEGORY =====================================

    // ============================ FOR SUB CATEGORY =====================================
    Route::get('/subcategory/list', [SubCategoryController::class, 'index'])->name('admin.subcategory.list');
    Route::get('/subcategory/create', [SubCategoryController::class, 'create'])->name('admin.subcategory.create');
    Route::post('/subcategory/create', [SubCategoryController::class, 'store'])->name('admin.subcategory.store');
    Route::get('/subcategory/edit/{id}', [SubCategoryController::class, 'edit'])->name('admin.subcategory.edit');
    Route::post('/subcategory/update', [SubCategoryController::class, 'update'])->name('admin.subcategory.update');
    // ============================ FOR SUB CATEGORY =====================================

    // ============================ FOR BRAND =====================================
    // Route::get('/brand/list', [BrandController::class, 'index'])->name('admin.brand.list');
    // Route::get('/brand/create', [BrandController::class, 'create'])->name('admin.brand.create');
    // Route::post('/brand/create', [BrandController::class, 'store'])->name('admin.brand.store');
    // Route::get('/brand/edit/{id}', [BrandController::class, 'edit'])->name('admin.brand.edit');
    // Route::post('/brand/update', [BrandController::class, 'update'])->name('admin.brand.update');
    // ============================ FOR BRAND =====================================

    // ============================ FOR PRODUCT =====================================
    Route::get('product/list', [ProductController::class, 'index'])->name('admin.product.list');
    Route::get('product/create', [ProductController::class, 'create'])->name('admin.product.create');
    Route::post('product/create', [ProductController::class, 'store'])->name('admin.product.store');
    Route::get('product/edit/{id}', [ProductController::class, 'edit'])->name('admin.product.edit');
    Route::post('product/update', [ProductController::class, 'update'])->name('admin.product.update');
    // ============================ FOR PRODUCT =====================================
    // ============================ FOR PRODUCT FILTER =====================================
    Route::get('product/filter', [FilterController::class, 'productFilter'])->name('admin.product.filter');
    // ============================ FOR PRODUCT FILTER =====================================

    // ============================ FOR SLIDER =====================================
    Route::get('slider/list', [SliderController::class, 'index'])->name('admin.slider.list');
    Route::get('slider/create', [SliderController::class, 'create'])->name('admin.slider.create');
    Route::post('slider/create', [SliderController::class, 'store'])->name('admin.slider.store');
    Route::get('slider/edit/{id}', [SliderController::class, 'edit'])->name('admin.slider.edit');
    Route::post('slider/update', [SliderController::class, 'update'])->name('admin.slider.update');
    // SLIDER SORTING ...
    Route::post('slider/sorting', [SliderController::class, 'sorting'])->name('admin.slider.sorting');
    // ============================ FOR SLIDER =====================================

    // ============================ FOR PRODUCT MULTIPLE IMAGE UPLOAD ================================
    Route::get('product/add-multi-pro-image/{id}', [ProductController::class, 'addMultiProImage'])->name('admin.product.multi_pro_img');
    Route::post('product/save-multi-pro-image', [ProductController::class, 'saveMultiProImage'])->name('admin.product.save_multi_pro_img');
    Route::get('product/active-product-img/{id}', [ProductController::class, 'activeProductImg'])->name('admin.product.active_product_img');
    Route::get('product/inactive-product-img/{id}', [ProductController::class, 'inactiveProductImg'])->name('admin.product.inactive_product_img');
    Route::get('product/delete-product-img/{id}', [ProductController::class, 'delete'])->name('admin.product.delete_product_img');
    // ============================ FOR PRODUCT MULTIPLE IMAGE UPLOAD ================================

    // ================================ FOR ORDER MANAGEMENT ========================================
    Route::get('product/order/pending', [OrderController::class, 'pendingOrder'])->name('admin.order.pending');
    Route::get('product/order/accept/{id}', [OrderController::class, 'acceptOrder'])->name('admin.order.accept');
    Route::get('product/order/cancel/{id}', [OrderController::class, 'cancelOrder'])->name('admin.order.cancel');
    Route::get('product/order/accepted', [OrderController::class, 'acceptedOrder'])->name('admin.order.accepted');
    Route::get('product/order/deliver/{id}', [OrderController::class, 'deliverOrder'])->name('admin.order.deliver');
    Route::get('product/order/delivered', [OrderController::class, 'deliveredOrder'])->name('admin.order.delivered');
    Route::get('product/order/cancelled', [OrderController::class, 'cancellededOrder'])->name('admin.order.cancelled');
    Route::get('product/order/invoice/{id}', [OrderController::class, 'invoice'])->name('admin.order.invoice');
    // ================================ FOR ORDER MANAGEMENT ========================================
    Route::get('test', [RealTimeBroadcastController::class, 'sendNotification']);

    // ========================= FOR CONTACT MAIL ==============================
    Route::get('contact-mail/index', [ContactUsMailController::class, 'index'])->name('admin.contact_mail.index');
    // ========================= FOR CONTACT MAIL ==============================
});

Route::get('/admin/login', [LoginController::class, 'login'])->name('admin.login');
Route::post('/admin/login', [LoginController::class, 'admin_login'])->name('admin.login_check');
