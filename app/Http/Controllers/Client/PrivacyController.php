<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Traits\CommonTrait;

class PrivacyController extends Controller
{
    use CommonTrait;
    public function index()
    {
        $categories = $this->getCategory(); //NOTE: FOR GETTING CATEGORIES ...
        return view('client.layouts.pages.privacy', compact ('categories'));

    }
}
