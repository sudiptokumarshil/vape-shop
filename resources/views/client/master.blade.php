<!DOCTYPE html>
<html lang="en">

<head>
    <title> @yield ('client_title') @yield('client_meta')</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-compatible" content="IE=edge">
    <meta name="description" content="@yield('client_meta')">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/') }}client/assets/images/favicon.png" />
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&amp;display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/animate.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/slick.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/chosen.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/magnific-popup.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/lightbox.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/js/fancybox/source/jquery.fancybox.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/jquery.scrollbar.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/mobile-menu.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/fonts/flaticon/flaticon.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/style.css">
    <style>
        .login-modal .modal-body {
            padding: 20px;
        }

        .close span {
            color: #770f12;
        }

        .close {
            float: right;
            font-size: 28px;
            font-weight: bold;
            line-height: 1;
            color: #000;
            text-shadow: 0 1px 0 #fff;
            opacity: 1;
        }

        .login-modal .modal-content {
            background-color: #060e32;
            color: white;
        }

        /*login*/

        /*user dropdown*/
        .drop-down.user-drop-down {
            display: inline-block;
            position: relative;
            z-index: 1000;
        }

        .drop-down__button {
            background: linear-gradient(to right, #0c1b63, #ed1d24);
            display: inline-block;
            line-height: 25px;
            padding: 0 13px;
            text-align: left;
            border-radius: 4px;
            box-shadow: 0px 4px 6px 0px rgba(0, 0, 0, 0.2);
            cursor: pointer;
        }

        .drop-down__name {
            font-size: 12px;
            text-transform: uppercase;
            color: #fff;
            font-weight: 400;
            letter-spacing: 2px;
        }


        .drop-down__menu-box {
            position: absolute;
            width: 100%;
            left: 0;
            background-color: #fff;
            border-radius: 4px;
            box-shadow: 0px 3px 6px 0px rgba(0, 0, 0, 0.2);
            transition: all 0.3s;
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            -ms-transition: all 0.3s;
            -o-transition: all 0.3s;
            visibility: hidden;
            opacity: 0;
            margin-top: 5px;
            /*padding: 0px 23px 7px 0px;*/
        }

        .drop-down__menu {
            margin: 0;
            padding: 0 0px;
            list-style: none;

        }

        .drop-down__menu-box:before {
            content: '';
            background-color: transparent;
            border-right: 8px solid transparent;
            position: absolute;
            border-left: 8px solid transparent;
            border-bottom: 8px solid #fff;
            border-top: 8px solid transparent;
            top: -15px;
            right: 18px;

        }

        .drop-down__menu-box:after {
            content: '';
            background-color: white;
        }

        .drop-down__item .svg-inline--fa {
            color: #127b3e !important;
        }

        .drop-down__item {
            font-size: 14px;
            padding: 0px 1px;
            margin: 5px;
            text-align: left;
            font-weight: 500;
            color: #0c1b63;
            cursor: pointer;
            position: relative;
            /*border-bottom: 1px solid #e0e2e9;*/
        }

        .drop-down__item-icon {
            width: 15px;
            height: 15px;
            position: absolute;
            right: 0px;
            fill: #8995b6;

        }

        .drop-down__item:hover .drop-down__item-icon {
            fill: #3d6def;
        }

        .drop-down__item:hover {
            color: #3d6def;
        }

        .drop-down__item:last-of-type {
            border-bottom: 0;
        }


        .drop-down--active .drop-down__menu-box {
            visibility: visible;
            opacity: 1;
            /*margin-top: 15px;*/
        }

        .drop-down__item a {
            color: #0e509f;
        }

        /*.drop-down__item:hover .bg-blue{*/

        /*}*/

        .drop-down__item:before {
            content: '';
            position: absolute;
            width: 3px;
            height: 21px;
            background-color: #ed1d24;
            left: -13px;
            top: 50%;
            transform: translateY(-50%);
            display: none;
        }

        .drop-down__item:hover:before {
            display: block;
        }

        /*user dropdown*/

        .ui-autocomplete {
            position: absolute;
            top: 100%;
            left: 0;
            z-index: 1000;
            float: left;
            display: none;
            min-width: 672px !important;
            padding: 0px 10px;
            margin: 0 0 10px 25px;
            list-style: none;
            background-color: #f5f8f9;
            font-family: 'Ubuntu', sans-serif;
            text-transform: initial !important;
            font-size: 14px;
        }

        .ui-menu-item {
            border-bottom: 1px solid #b4c4d4 !important;
            padding-bottom: 10px !important;
            padding-top: 10px !important;
        }

        .ui-menu-item>a.ui-corner-all {
            display: block;
            clear: both;
            font-weight: normal;
            line-height: 25px;
            color: #0a0a0a;
            white-space: nowrap;
            text-decoration: none;
            text-transform: lowercase !important;
        }

        .ui-menu-item>a.ui-corner-all:first-letter {
            text-transform: uppercase;
        }

        .ui-state-hover,
        .ui-state-active {
            color: #ffffff;
            text-decoration: none;
            background-color: #0088cc;
            padding-left: 0px !important;
            padding-right: 0px !important;
            border-radius: 0px;
            -webkit-border-radius: 0px;
            -moz-border-radius: 0px;
            background-image: none;
        }

        .ui-state-highlight a {
            font-weight: bold;
            color: #002e5b !important;
        }

        .ui-state-highlight {
            font-weight: bold;
            color: #002e5b !important;
        }

        .navbar {
            background: #0c1b63 !important;
            box-shadow: none;
            margin-bottom: 0px;
        }

        .head-logo.mobile {
            height: 70px;
        }

        .logo-green.mobile {
            font-size: 13px;
        }

        .desktop-nav .dropdown-toggle::after {
            right: -2px;
        }

        .badge.mobile {
            font-size: 13px;
            padding: 2px 5px;
        }

        #sidebar {
            background: #0d509f;
        }

        #sidebar .sidebar-header {
            padding: 20px;
            background: #ffffff;
        }

        #dismiss {
            background: none;
            color: #ed1d24;
        }

        .btn-info {
            color: #0e509f;
            background: none;
            border: none;
            font-size: 24px;
        }

        .btn-info:hover {
            color: #ed1d24 !important;
            background: none !important;
            border: none
        }

        .collapsible {
            background-color: #0e509f;
            color: white;
            cursor: pointer;
            padding: 4px;
            width: 100%;
            border: none;
            text-align: left;
            outline: none;
            font-size: 20px;
        }

        #sidebar .active,
        .collapsible:hover {
            background-color: #093c79;
        }

        .collapsible:after {
            content: '\002B';
            color: white;
            font-weight: bold;
            float: right;
            margin-right: 17px;
        }

        .collapsible.active:after {
            content: "\2212";
        }

        #sidebar ul ul button {
            font-size: 0.9em !important;
            padding-left: 30px !important;
            background: #0d509e;
        }

        #sidebar ul li button:hover {
            color: #ffffff;
            background: #0d509e;
            border: none;
        }

        button:focus {
            outline: none;
        }

        .show-mega-menu {
            padding: 0 0px;
            max-height: 0;
            overflow: hidden;
            transition: max-height 0.1s ease-out;
        }

        ul.show-mega-menu li a {
            margin-left: 5px !important;
            background: #0d509e00 !important;
        }

        ul.show-mega-menu li {
            list-style-type: none;
        }

        ul.show-mega-menu li a:hover {
            background: none;
            color: white;
        }

        @media screen and (max-width: 767px) {


            .drop-down__name {
                font-size: 9px;
                letter-spacing: 1px;
            }

            .drop-down__button {
                line-height: 22px;
            }
        }

        .product-item .outOfStock {
            font-size: 11px;
            text-transform: uppercase;
            font-weight: 700;
            display: inline-block;
            float: right;
            line-height: 24px;
            height: 24px;
            min-width: 50px;
            padding: 0 5px;
            text-align: center;
            background-color: red;
            color: #fff;
            position: relative;
        }
    </style>
</head>

<body class="home">
    @include('client.layouts.header')
    <div class="">
        <div class="fullwidth-template">
            @yield('home')
        </div>
    </div>
    @include('client.layouts.footer')
    <script src="{{ asset('/') }}client/assets/js/jquery-1.12.4.min.js"></script>
    <script src="{{ asset('/') }}client/assets/js/jquery.plugin-countdown.min.js"></script>
    <script src="{{ asset('/') }}client/assets/js/jquery-countdown.min.js"></script>
    <script src="{{ asset('/') }}client/assets/js/bootstrap.min.js"></script>
    <script src="{{ asset('/') }}client/assets/js/owl.carousel.min.js"></script>
    <script src="{{ asset('/') }}client/assets/js/magnific-popup.min.js"></script>
    <script src="{{ asset('/') }}client/assets/js/isotope.min.js"></script>
    <script src="{{ asset('/') }}client/assets/js/jquery.scrollbar.min.js"></script>
    <script src="{{ asset('/') }}client/assets/js/jquery-ui.min.js"></script>
    <script src="{{ asset('/') }}client/assets/js/mobile-menu.js"></script>
    <script src="{{ asset('/') }}client/assets/js/chosen.min.js"></script>
    <script src="{{ asset('/') }}client/assets/js/slick.js"></script>
    <script src="{{ asset('/') }}client/assets/js/jquery.elevateZoom.min.js"></script>
    <script src="{{ asset('/') }}client/assets/js/jquery.actual.min.js"></script>
    <script src="{{ asset('/') }}client/assets/js/fancybox/source/jquery.fancybox.js"></script>
    <script src="{{ asset('/') }}client/assets/js/lightbox.min.js"></script>
    <script src="{{ asset('/') }}client/assets/js/owl.thumbs.min.js"></script>
    <script src="{{ asset('/') }}client/assets/js/jquery.scrollbar.min.js"></script>
    <script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyC3nDHy1dARR-Pa_2jjPCjvsOR4bcILYsM'></script>
    <script src="{{ asset('/') }}client/assets/js/frontend-plugin.js"></script>
    <script src="{{ asset('/') }}client/assets/js/custom/link.js"></script>
    <script src="{{ asset('/') }}client/assets/js/custom/cart.js"></script>
    <script src="{{ asset('/') }}client/assets/js/custom/message.js"></script>
    <script src="{{ asset('/') }}client/assets/js/custom/common.js"></script>
    <script src="{{ asset('/') }}client/assets/js/custom/search.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    @yield('front_script')
</body>

<!-- Mirrored from ledthanhdat.vn/html/herald/home2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 13 Mar 2022 05:21:59 GMT -->

</html>
