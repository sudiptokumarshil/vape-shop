<nav class="sidebar">
    <div class="logo d-flex justify-content-between">
        <a href="{{ route('admin.home') }}">
            <h5>Vape Fish Dubai</h5>
        </a>
        <div class="sidebar_close_icon d-lg-none">
            <i class="ti-close"></i>
        </div>
    </div>
    <ul id="sidebar_menu">
        <li class="side_menu_title">
            <a href="{{ route('admin.home') }}" aria-expanded="false">Dashboard</a>
        </li>

        <li class="side_menu_title">
            <a class="has-arrow" href="#" aria-expanded="false">
                <img src="img/menu-icon/2.svg" alt="">
                <span>Category</span>
            </a>
            <ul>
                <li><a href="{{ route('admin.category.create') }}">Create Category</a></li>
                <li><a href="{{ route('admin.category.list') }}">Manage Category</a></li>
            </ul>
        </li>
        <li class="side_menu_title">
            <a class="has-arrow" href="#" aria-expanded="false">
                <img src="img/menu-icon/2.svg" alt="">
                <span>Slider</span>
            </a>
            <ul>
                <li><a href="{{ route('admin.slider.create') }}">Create Slider</a></li>
                <li><a href="{{ route('admin.slider.list') }}">Manage Slider</a></li>
            </ul>
        </li>
        <li class="side_menu_title">
            <a class="has-arrow" href="#" aria-expanded="false">
                <img src="img/menu-icon/3.svg" alt="">
                <span>Product</span>
            </a>
            <ul>
                <li><a href="{{ route('admin.product.create') }}">Add Product</a></li>
                <li><a href="{{ route('admin.product.list') }}">Manage Product</a></li>
            </ul>
        </li>
        <li class="side_menu_title">
            <a class="has-arrow" href="#" aria-expanded="false">
                <img src="img/menu-icon/3.svg" alt="">
                <span>Order</span>
            </a>
            <ul>
                <li><a href="{{ route('admin.order.pending') }}">Pending</a></li>
                <li><a href="{{ route('admin.order.accepted') }}">Processing</a></li>
                <li><a href="{{ route('admin.order.delivered') }}">Delivered</a></li>
                <li><a href="{{ route('admin.order.cancelled') }}">Cancelled</a></li>
            </ul>
        </li>
        <li class="side_menu_title">
            <a class="has-arrow" href="#" aria-expanded="false">
                <img src="img/menu-icon/3.svg" alt="">
                <span>Contact Mail</span>
            </a>
            <ul>
                <li><a href="{{ route('admin.contact_mail.index') }}">Inbox</a></li>
            </ul>
        </li>
    </ul>
</nav>
