<?php

namespace App\Traits;

use App\Models\Category;
use Intervention\Image\ImageManagerStatic as Image;

trait CommonTrait
{
    public function saveImageOrFile($filePath, $fileName, $existingImage = null)
    {
        if ($fileName) {
            if (!empty($existingImage)) {
                @unlink(public_path() . '/' . $filePath . $existingImage);
            }
            $image = $fileName;
            $imageName = $image->getClientOriginalExtension();
            $newName = date('YmdHis') . rand(100, 1000) . '.' . $imageName;
            $directory =  public_path() . '/' . $filePath;
            $image->move($directory, $newName);
            return $newName;
        }
    }
    public function saveImageWithCrop($filePath, $fileName, $existingImage = null)
    {
        if ($fileName) {
            if (!empty($existingImage)) {
                @unlink(public_path() . '/' . $filePath . $existingImage);
            }
            $image = $fileName;
            $imageName = $image->getClientOriginalExtension();
            $newName = date('YmdHis') . rand(100, 1000) . '.' . $imageName;
            $directory =  public_path() . '/' . $filePath;
            $imageUrl = $directory . $newName;
            Image::make($image)->resize(1920, 550)->save($imageUrl);
            return $newName;
        }
    }


    public function getCategory()
    {
        $category = Category::select('id', 'name', 'root_id', 'status','sorting')
            ->with(['get_subcategory' => function ($query) {
                $query->where('status', 1);
            }, 'get_brand' => function ($query) {
                $query->where('status', 1);
            }, 'get_flovour' => function ($query) {
                $query->where('status', 1);
            },])
            ->whereNotIn('id', ['19']) // 19 = Delivery Info...
            ->where('root_id', null)
            ->where('status', 1)
            ->orderBy('sorting','asc')
            ->get();
        return $category;
    }

    public function getBrand()
    {
        $brand = Category::select('id', 'name', 'root_id', 'status', 'type')
            ->where('type', 2)
            ->where('status', 1)
            ->get();
        return $brand;
    }

    public function getFlavour()
    {
        $flavour = Category::select('id', 'name', 'root_id', 'status', 'type')
            ->where('type', 3)
            ->where('status', 1)
            ->get();
        return $flavour;
    }

    public function formatedString($str)
    {
        return preg_replace("/\s+/", "", $str);
    }
}
