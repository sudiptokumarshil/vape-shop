@extends('admin.master')
@section('titles')
    Brand
@endsection
@section('admin_home')
    <div class="QA_section">
        <div class="white_box_tittle list_header">
            <h4>Brand</h4>
            <div class="box_right d-flex lms_block">
                <div class="serach_field_2">

                </div>
                <div class="add_button ml-10">
                    <a href="{{ route('admin.brand.create') }}" class="btn_1">Add
                        Brand</a>
                </div>
            </div>
        </div>
        <div class="QA_table mb_30">
            <table class="table lms_table_active">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php($i = 1)
                    @foreach ($brands as $brand)
                        <tr>
                            <th scope="row"> {{ $i++ }}</th>
                            <td>{{ $brand->name }}</td>
                            <td>
                                @if ($brand->status == 1)
                                    Active
                                @else
                                    Inactive
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('admin.brand.edit', ['id' => $brand->id]) }}" type="button"
                                    class="btn btn-warning">Edit</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="d-flex justify-content-center">
                {!! $brands->links() !!}
            </div>
        </div>
    </div>
@endsection
