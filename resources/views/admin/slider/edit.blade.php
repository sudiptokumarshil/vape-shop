@extends('admin.master')
@section('titles')
    Slider
@endsection
@section('admin_home')
    <div class="white_box mb_30">
        <div class="box_header ">
            <div class="main-title">
                <h3 class="mb-0">Update Slider</h3>
            </div>
        </div>
        @include('admin.layouts.message')
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <form action="{{ route('admin.slider.update') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" value="{{ $edit->title }}" required name="title" class="form-control" />
                        <input type="hidden" name="id" value="{{ $edit->id }}" />
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea name="description" required class="form-control">{{ $edit->description }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Image (Image size should be 1920 in width 550 in height and 96 dpi.)</label>
                        <input type="file" id="img" name="image" class="form-control" />
                        <br>
                        <img src="{{ sliderImagePath($edit->image) }}" style="height: 50%;width:50%" id="blah" alt="">
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select name="status" class="form-control">
                            <option value="1" {{ $edit->status == 1 ? 'selected' : '' }}>Active</option>
                            <option value="2" {{ $edit->status == 2 ? 'selected' : '' }}>Inactive</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm">Update</button>
                    <a href="{{ route('admin.slider.list') }}" class="btn btn-info btn-sm">Go To List</a>
                </form>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        const readURL = (input) => {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#blah').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
            $('#blah').show();
        }
        $("#img").change(function() {
            readURL(this);
        });
    </script>
@endsection
