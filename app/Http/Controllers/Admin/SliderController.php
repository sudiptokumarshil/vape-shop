<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use App\Traits\CommonTrait;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    use CommonTrait;
    public function index()
    {
        $sliders = Slider::orderBy('id', 'desc')->paginate(10);
        return view('admin.slider.list', compact('sliders'));
    }
    public function create()
    {
        return view('admin.slider.create');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|max:7000',
        ]);
        $slider = new Slider();
        $slider->title = $request->title;
        $slider->description = $request->description;
        $slider->image =  $this->saveImageWithCrop('admin/slider/', $request->file('image'));
        $slider->save();
        return redirect()->back()->with('success', 'Slider Saved Successfully!');
    }
    public function edit($id)
    {
        $edit = Slider::find($id);
        return view('admin.slider.edit', compact('edit'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'image' => 'max:7000',
        ]);
        $slider = Slider::find($request->id);
        $slider->title = $request->title;
        $slider->description = $request->description;
        $slider->status = $request->status;
        if ($request->file('image')) {
            $slider->image =  $this->saveImageWithCrop('admin/slider/', $request->file('image'), $slider->image);
        }
        $slider->update();
        return redirect()->back()->with('success', 'Slider Updated Successfully!');
    }

    public function sorting(Request $request)
    {
        $sort = Slider::find($request->id);
        $sort->sorting = $request->value;
        $sort->update();
        return response()->json(["message" => "Success!"]);
    }
}
