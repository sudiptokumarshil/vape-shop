<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductColor;
use App\Models\ProductImage;
use App\Traits\CommonTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    use CommonTrait;
    public function index()
    {
        $products = Product::with(['get_category:id,name', 'get_subcategory:id,name', 'get_brand:id,name', 'get_color'])
            ->orderBy('id', 'desc')
            ->paginate(10);
        $categories = Category::select('id', 'name', 'status', 'root_id')
            ->where('root_id', null)
            ->get();
        return view('admin.product.list', [
            'products' => $products,
            'categories' => $categories
        ]);
    }

    public function create()
    {
        $categories = Category::select('id', 'name', 'status', 'root_id')
            ->where('root_id', null)
            ->get();
        return view('admin.product.create', compact('categories'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'name' => 'required',
            'category_id' => 'required',
            'brand_id' => 'required',
            'meta' => 'required|unique:products,meta',
            'title' => 'required|unique:products,title',
            'url' => 'required|unique:products,url',
        ]);

        DB::beginTransaction();

        try {
            $product = new Product();
            $product->name = $request->name;
            $product->category_id = intval($request->category_id);
            $product->subcategory_id = intval($request->subcategory_id);
            $product->brand_id = intval($request->brand_id);
            $product->flavour_id = intval($request->flavour_id);
            $product->price = floatval($request->price);
            $product->discount = floatval($request->discount);
            $product->stock = intval($request->stock);
            $product->is_stock = $request->is_stock;
            $product->description = $request->description;
            $product->short_description = $request->short_description;
            $product->status = intval($request->status);
            $product->title = $request->title;
            $product->meta = $request->meta;
            $product->url = $this->formatedString($request->url);

            if (!empty($request->image[0])) {
                $product->image =  $this->saveImageOrFile('admin/product/', $request->image[0]);
            }
            $product->save();

            if (!empty($request->image[0])) {
                foreach (array_slice($request->image, 1) as $key => $value) {
                    $productImage = new ProductImage();
                    $productImage->product_id = $product->id;
                    $productImage->image =  $this->saveImageOrFile('admin/product/', $value);
                    $productImage->save();
                }
            }

            if (!empty($request->color[0])) {
                foreach ($request->color as $key => $color) {
                    if (isset($key)) {
                        $productColor = new ProductColor();
                        $productColor->product_id = $product->id;
                        $productColor->name = $color;
                        $productColor->color_image = $this->saveImageOrFile('admin/product/', $request['color_image'][$key]);
                        $productColor->save();
                    }
                }
            }

            DB::commit();
            return redirect()->back()->with('success', 'Product Saved Successfully!!');
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function edit($id)
    {
        $product = Product::with(['get_subcategory', 'get_color'])->find($id);
        $categories = Category::select('id', 'name', 'status', 'root_id')
            ->where('root_id', null)
            ->get();
        $subCategories = Category::select('id', 'name', 'status', 'root_id', 'type')
            ->where('root_id', $product->category_id)
            ->where('type', 1)
            ->where('status', 1)
            ->get();
        $brands = Category::select('id', 'name', 'status', 'root_id', 'type')
            ->where('root_id', $product->category_id)
            ->where('type', 2)
            ->where('status', 1)
            ->get();
        $flavours = Category::select('id', 'name', 'status', 'root_id', 'type')
            ->where('root_id', $product->category_id)
            ->where('type', 3)
            ->where('status', 1)
            ->get();
        return view('admin.product.edit', [
            'product' => $product,
            'categories' => $categories,
            'brands' => $brands,
            'subCategories' => $subCategories,
            'flavours' => $flavours,
        ]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'category_id' => 'required',
            'brand_id' => 'required',
            'image'  =>  'max:2048',
            'meta' => 'required|unique:products,meta,' . $request->id,
            'title' => 'required|unique:products,title,' . $request->id,
            'url' => 'required|unique:products,url,' . $request->id,
        ]);
        DB::beginTransaction();

        try {

            $product = Product::find($request->id);
            $product->name = $request->name;
            $product->category_id = intval($request->category_id);
            $product->subcategory_id = intval($request->subcategory_id);
            $product->brand_id = intval($request->brand_id);
            $product->flavour_id = intval($request->flavour_id);
            $product->price = floatval($request->price);
            $product->discount = floatval($request->discount);
            $product->stock = intval($request->stock);
            $product->description = $request->description;
            $product->short_description = $request->short_description;
            $product->is_stock = $request->is_stock;
            $product->status = intval($request->status);
            $product->title = $request->title;
            $product->meta = $request->meta;
            $product->url = $this->formatedString($request->url);

            if ($request->file('image')) {
                $product->image =  $this->saveImageOrFile('admin/product/', $request->file('image'), $product->image);
            }

            $product->update();

            ProductColor::where('product_id', $product->id)->whereNotIn('id', $request->color_id)->delete();
            if (!empty($request->color[0])) {
                foreach ($request->color as $key => $color) {
                    $update = ProductColor::where('id', $request->color_id[$key])->first();
                    if (isset($key)) {
                        if (!empty($update)) {
                            $update->name = $color;
                            if (!empty($request['color_image'][$key])) {
                                $update->color_image = $this->saveImageOrFile('admin/product/', $request['color_image'][$key], $update->color_image);
                            }
                            $update->update();
                        } else {
                            $productColor = new ProductColor();
                            $productColor->product_id = $product->id;
                            $productColor->name = $color;
                            $productColor->color_image = $this->saveImageOrFile('admin/product/', $request['color_image'][$key]);
                            $productColor->save();
                        }
                    }
                }
            }

            DB::commit();
            return redirect()->back()->with('success', 'Product Updated Successfully!!');
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function addMultiProImage($id)
    {
        $product = Product::select('id', 'name')->where('id', $id)->first();
        $productImage = ProductImage::select('id', 'image', 'status', 'product_id')->where('product_id', $id)->get();
        return view('admin.product.create_multi_image', [
            'product' => $product,
            'productImage' => $productImage,
        ]);
    }

    public function saveMultiProImage(Request $request)
    {
        if (!empty($request->product_image[0])) {
            foreach ($request->product_image as $key => $value) {
                $productImage = new ProductImage();
                $productImage->product_id = $request->product_id;
                $productImage->image =  $this->saveImageOrFile('admin/product/', $value);
                $productImage->save();
            }
            return redirect()->back()->with('success', 'Product Image Saved Successfully!!');
        } else {
            return redirect()->back()->with('success', 'Sorry! You did not insert any image.');
        }
    }

    public function activeProductImg($id)
    {
        $productImage = ProductImage::find($id);
        $productImage->status = 1;
        $productImage->update();
        return redirect()->back()->with('success', 'Product Image Activated Successfully!!');
    }

    public function inactiveProductImg($id)
    {
        $productImage = ProductImage::find($id);
        $productImage->status = 2;
        $productImage->update();
        return redirect()->back()->with('success', 'Product Image Deactivated Successfully!!');
    }

    public function delete($id)
    {
        $delete = ProductImage::find(getDecrypt($id));
        if (!empty($delete->image)) {
            @unlink(public_path() . '/' . 'admin/product/' . $delete->image);
        }
        $delete->delete();
        return redirect()->back()->with('success', 'Product Deleted Successfully!!');
    }
}
