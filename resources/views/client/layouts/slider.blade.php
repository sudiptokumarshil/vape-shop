<div class="home-slider fullwidth">
    <div class="slider-owl owl-slick equal-container nav-center equal-container"
        data-slick='{"autoplay":true, "autoplaySpeed":10000, "arrows":true, "dots":true, "infinite":true, "speed":800, "rows":1}'
        data-responsive='[{"breakpoint":"2000","settings":{"slidesToShow":1}}]'>
        @if (!empty($sliders[0]))
            <div class="slider-item style4">
                <div class="slider-inner equal-element"
                    style="background-image: url({{ sliderImagePath($sliders[0]->image) }}); border-radius: 0; background-repeat: no-repeat;width: 100%;height: 100%">
                    <div class="container">
                        <div class="slider-infor">
                            <h5 class="title-small">
                                {{ $sliders[0]->title }}
                            </h5>
                            <h3 class="title-big">
                                {{ $sliders[0]->description }}
                            </h3>
                            {{-- <div class="price">
                                New Price:
                                <span class="number-price">
                                    $25.00
                                </span>
                            </div>
                            <a href="#" class="button btn-shop-the-look bgroud-style">Shop now</a> --}}
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="slider-item style4">
                <div class="slider-inner equal-element"
                    style="background-image: url(client/assets/images/slider-thumb-full-1.jpg); border-radius: 0; background-repeat: no-repeat;width: 100%;height: 100%">
                    <div class="container">
                        <div class="slider-infor">
                            <h5 class="title-small">
                                Vape Fish Dubai
                            </h5>
                            <h3 class="title-big">
                                Vape Fish Dubai
                                <br />
                                {{-- 4 colors --}}
                            </h3>
                            <div class="price">
                                {{-- New Price: --}}
                                <span class="number-price">
                                    {{-- $25.00 --}}
                                </span>
                            </div>
                            {{-- <a href="#" class="button btn-shop-the-look bgroud-style">Shop now</a> --}}
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @foreach ($sliders as $key => $slider)
            @if ($key != 0)
                <div class="slider-item style5">
                    <div class="slider-inner equal-element"
                        style="background-image: url({{ sliderImagePath($slider->image) }}); border-radius: 0; background-repeat: no-repeat;width: 100%;height: 100%">
                        <div class="container">
                            <div class="slider-infor">
                                <h5 class="title-small">
                                    {{ $slider->title }}
                                </h5>
                                <h3 class="title-big">
                                    {{ $slider->description }}
                                </h3>
                                {{-- <div class="when-code">
                            When Use Code:
                            <span class="number-code">
                                HERALD
                            </span>
                        </div> --}}
                                {{-- <a href="#" class="button btn-view-promotion bgroud-style">VIEW ALL</a> --}}
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
        {{-- <div class="slider-item style6">
            <div class="slider-inner equal-element"
                style="background-image: url(client/assets/images/slider-thumb-full-3.jpg); border-radius: 0; background-repeat: no-repeat;width: 100%;height: 100%">
                <div class="container">
                    <div class="slider-infor">
                        <h5 class="title-small">
                            Make your style Difference!
                        </h5>
                        <h3 class="title-big">
                            Let’s create<br />
                            Your own style
                        </h3>
                        <div class="price">
                            Template Price:
                            <span class="number-price">
                                $89.00
                            </span>
                        </div>
                        <a href="#" class="button btn-lets-create bgroud-style">Let’s Create</a>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
