@extends('admin.master')
@section('titles')
    Product
@endsection
@section('admin_home')
    <div class="QA_section">
        <div class="white_box_tittle list_header">
            <h4>Product</h4>
            <div class="box_right d-flex lms_block">
                <div class="serach_field_2">

                </div>
                <div class="add_button ml-10">
                    <a href="{{ route('admin.product.create') }}" class="btn_1">Add
                        Product</a>
                </div>
            </div>
        </div>
        <div class="QA_table mb_30">
            <form action="{{ route('admin.product.filter') }}" method="GET">
                @csrf
                <div class="row mb-3">
                    <div class="col-md-1">

                    </div>
                    <div class="col-md-2">
                        <label>Product Name</label>
                        <input type="text" class="form-control input" style="height: calc(1.5em + .15rem + 2px);" name="product_name" />
                    </div>
                    <div class="col-md-2">
                        <label>Category Name</label>
                        <select name="category_id" class="form-control select_2" id="category"
                            onchange="getSubcategory(this.value)">
                            <option value="0">Choose One</option>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label>Subcategory Name</label>
                        <select name="subcategory_id" id="subCateId" class="form-control select_2">
                            <option value="0">Choose One</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label>Brand Name</label>
                        <select name="brand_id" id="brandId" class="form-control select_2">
                            <option value="0">Choose One</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label>Status</label>
                        <select name="status" class="form-control select_2" id="status">
                            <option value="0">Choose One</option>
                            <option value="1">Active</option>
                            <option value="2">Inactive</option>
                        </select>
                    </div>
                    <div class="col-md-1">

                    </div>

                </div>
                <div class="d-flex justify-content-center mb-3">
                    <button type="submit" class="btn btn-primary btn-sm">Search</button>
                </div>
            </form>
            <table class="table lms_table_active cls">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Category</th>
                        <th scope="col">Subcategory</th>
                        <th scope="col">Brand</th>
                        <th scope="col">Discount</th>
                        <th scope="col">Price</th>
                        <th scope="col">Colors</th>
                        <th scope="col">Image</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php($i = 1)
                    @foreach ($products as $product)
                        <tr>
                            <th scope="row"> {{ $i++ }}</th>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->get_category ? $product->get_category->name : 'Not Found' }}</td>
                            <td>{{ $product->get_subcategory ? $product->get_subcategory->name : 'Not Found' }}</td>
                            <td>{{ $product->get_brand ? $product->get_brand->name : 'Not Found' }}</td>
                            <td>{{ $product->discount }}</td>
                            <td>{{ $product->price }}</td>
                            <td>
                                @foreach ($product->get_color as $color)
                                    <span style="color:{{ $color->name }}">{{ $color->name }}</span>
                                @endforeach
                            </td>
                            <td>
                                <img src="{{ productImagePath($product->image) }}" alt="" style="height: 200" />
                            </td>
                            <td>
                                @if ($product->status == 1)
                                    Active
                                @else
                                    Inactive
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('admin.product.edit', ['id' => $product->id]) }}" type="button"
                                    class="btn btn-warning btn-sm">Edit</a>
                                <a href="{{ route('admin.product.multi_pro_img', ['id' => $product->id]) }}"
                                    type="button" class="btn btn-success btn-sm">Add Image</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="d-flex justify-content-center">
                {!! $products->links() !!}
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        // NOTE: FOR GETTING CATEGORY WISE SUBCATEGORY ...
        const getSubcategory = (id) => {
            $.ajax({
                url: "/admin/category/get-subcategory/" + id,
                type: "GET",
                data: {
                    _token: "{{ csrf_token() }}",
                },
                success: function(data) {
                    $('#subCateId').empty();
                    $('#brandId').empty();
                    $('#flavourId').empty();
                    let dataHtml = "";
                    let dataBHtml = "";
                    let dataFHtml = "";
                    dataHtml += `<option value='0'>Choose One</option>`;
                    $.each(data.subCategory, function(index, item) {
                        dataHtml += `<option value='${item.id}'>${item.name}</option>`;
                    });
                    dataBHtml += `<option value='0'>Choose One</option>`;
                    $.each(data.brands, function(index, item) {
                        dataBHtml += `<option value='${item.id}'>${item.name}</option>`;
                    });
                    dataFHtml += `<option value='0'>Choose One</option>`;
                    $.each(data.flavours, function(index, item) {
                        dataFHtml += `<option value='${item.id}'>${item.name}</option>`;
                    });
                    $('#subCateId').html(dataHtml);
                    $('#brandId').html(dataBHtml);
                    $('#flavourId').html(dataFHtml);
                },
            });
        }
    </script>
@endsection
