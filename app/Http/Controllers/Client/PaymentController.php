<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Traits\CommonTrait;

class PaymentController extends Controller
{
    use CommonTrait;
    public function index()
    {
        $categories = $this->getCategory(); //NOTE: FOR GETTING CATEGORIES ...
        return view('client.layouts.pages.payment', compact ('categories'));

    }
}
