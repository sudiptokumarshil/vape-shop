@extends('admin.master')
@section('titles')
    Contact Mail
@endsection
@section('admin_home')
    <div class="QA_section">
        <div class="white_box_tittle list_header">
            <h4>Contact Mail</h4>
            <div class="box_right d-flex lms_block">
            </div>
        </div>
        @include('admin.layouts.message')
        <div class="QA_table mb_30">
            <table class="table lms_table_active">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Customer Name</th>
                        <th scope="col">Customer Email</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Company</th>
                        <th scope="col">Message</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody id="contact-mail">
                    @foreach ($mails as $key => $mail)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $mail->name }}</td>
                            <td>{{ $mail->email }}</td>
                            <td>{{ $mail->phone }}</td>
                            <td>{{ $mail->company }}</td>
                            <td>{{ $mail->message }}</td>
                            <td>{{ $mail->created_at->diffForHumans() }}</td>
                            <td>

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="d-flex justify-content-center">

            </div>
        </div>
    </div>
@endsection
