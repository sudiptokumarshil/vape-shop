<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    public function index()
    {
        $categories = Category::with(['get_category'])
            ->orderBy('id', 'desc')
            ->where('root_id', '<>', null)
            ->where('type', '>', 0)
            ->paginate(5);
        return view('admin.sub_category.list', [
            'categories' => $categories
        ]);
    }

    public function create()
    {
        $categories = Category::orderBy('id', 'desc')->where('root_id', null)->get();
        return view('admin.sub_category.create', [
            'categories' => $categories,
        ]);
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            // 'name' => 'required||unique:categories,name',
            'category_id' => 'required'
        ]);
        $category = new Category();
        $category->name = $request->name;
        $category->root_id = $request->category_id;
        $category->status = $request->status;
        $category->type = $request->type;
        $category->save();
        return redirect()->route('admin.subcategory.list')->with('success', 'Sub Category Saved Successfully!!');
    }

    public function edit($id)
    {
        $category = Category::find($id);
        $categories = Category::orderBy('id', 'desc')
            ->where('root_id', null)
            ->get();
        return view('admin.sub_category.edit', [
            'subcategory' => $category,
            'categories' => $categories,
        ]);
    }
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            // 'name' => 'required|unique:categories,name,' . $request->id,
            'category_id' => 'required'
        ]);
        $category = Category::find($request->id);
        $category->name = $request->name;
        $category->root_id = $request->category_id;
        $category->type = $request->type;
        $category->status = $request->status;
        $category->update();
        return redirect()->route('admin.subcategory.list')->with('success', 'Sub Category Updated Successfully!!');
    }
}
