@extends('client.master')
@section('client_title')
    Contact Us
@endsection
@section('home')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-trail breadcrumbs">
                    <ul class="trail-items breadcrumb">
                        <li class="trail-item trail-begin">
                            <a href="index.html">Home</a>
                        </li>
                        <li class="trail-item trail-end active">
                            Contact us
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="content-area content-contact col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="site-main">
                    <h3 class="custom_blog_title">Contact us</h3>
                    @include('admin.layouts.message')
                </div>
            </div>
        </div>
    </div>
    <div class="page-main-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-contact">
                        <div class="col-lg-8 no-padding">
                            <div class="form-message">
                                <h2 class="title">
                                    Send us a Message!
                                </h2>
                                <form action="{{ route('send_message') }}" class="herald-contact-fom" method="POST">
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p>
                                                <span class="form-label">Your Name *</span>
                                                <span class="form-control-wrap your-name">
                                                    <input title="your-name" required type="text" name="name"
                                                        size="40" class="form-control form-control-name">
                                                </span>
                                            </p>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>
                                                <span class="form-label">
                                                    Your Email *
                                                </span>
                                                <span class="form-control-wrap your-email">
                                                    <input title="your-email" required type="email" name="email"
                                                        size="40" class="form-control form-control-email">
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p>
                                                <span class="form-label">Phone</span>
                                                <span class="form-control-wrap your-phone">
                                                    <input title="your-phone" required type="text" name="phone"
                                                        class="form-control form-control-phone">
                                                </span>
                                            </p>
                                        </div>
                                        <div class="col-sm-6">
                                            <p>
                                                <span class="form-label">
                                                    Company (Optional)
                                                </span>
                                                <span class="form-control-wrap your-company">
                                                    <input title="your-company" type="text" name="company"
                                                        class="form-control your-company">
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                    <p>
                                        <span class="form-label">
                                            Your Message
                                        </span>
                                        <span class="wpcf7-form-control-wrap your-message">
                                            <textarea title="your-message" required name="message" cols="40" rows="9" class="form-control your-textarea"></textarea>
                                        </span>
                                    </p>
                                    <p>
                                        <input type="submit" value="SEND MESSAGE"
                                            class="form-control-submit button-submit">
                                    </p>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-4 no-padding">
                            <div class="form-contact-information">
                                <form action="#" class="herald-contact-info">
                                    <h2 class="title">
                                        Contact information
                                    </h2>
                                    <div class="info">
                                        <div class="item address">
                                            <span class="icon">

                                            </span>
                                            <span class="text">
                                                Al Barsha , Dubai
                                            </span>
                                        </div>
                                        <div class="item phone">
                                            <a href="http://wa.me/+8801827298568">
                                                <span class="icon">

                                                </span>
                                                <span class="text">
                                                    +88 01827-298 568
                                                </span>
                                            </a>
                                        </div>
                                        <div class="item email">
                                            <a href="mail:info@vapefishdubai.com">
                                                <span class="icon">

                                                </span>
                                                <span class="text">
                                                    info@vapefishdubai.com
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="socials">
                                        <a href="https://www.facebook.com/vapefishdubai/" class="social-item"
                                            target="_blank">
                                            <span class="icon fa fa-facebook">

                                            </span>
                                        </a>
                                        <a href="https://www.instagram.com/vape_fish_/" class="social-item" target="_blank">
                                            <span class="icon fa fa-instagram">

                                            </span>
                                        </a>
                                        <a href="#" class="social-item" target="_blank">
                                            <span class="icon fa fa-twitter-square">

                                            </span>
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
