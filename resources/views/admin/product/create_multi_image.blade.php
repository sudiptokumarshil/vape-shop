@extends('admin.master')
@section('titles')
    Product
@endsection
@section('admin_home')
    <style>
        html * {
            box-sizing: border-box;
        }

        p {
            margin: 0;
        }

        .upload {
            &__box {
                padding: 40px;
            }

            &__inputfile {
                width: .1px;
                height: .1px;
                opacity: 0;
                overflow: hidden;
                position: absolute;
                z-index: -1;
            }

            &__btn {
                display: inline-block;
                font-weight: 600;
                color: #fff;
                text-align: center;
                min-width: 116px;
                padding: 5px;
                transition: all .3s ease;
                cursor: pointer;
                border: 2px solid;
                background-color: #4045ba;
                border-color: #4045ba;
                border-radius: 10px;
                line-height: 26px;
                font-size: 14px;

                &:hover {
                    background-color: unset;
                    color: #4045ba;
                    transition: all .3s ease;
                }

                &-box {
                    margin-bottom: 10px;
                }
            }

            &__img {
                &-wrap {
                    display: flex;
                    flex-wrap: wrap;
                    margin: 0 -10px;
                }

                &-box {
                    width: 200px;
                    padding: 0 10px;
                    margin-bottom: 12px;
                }

                &-close {
                    width: 24px;
                    height: 24px;
                    border-radius: 50%;
                    background-color: rgba(0, 0, 0, 0.5);
                    position: absolute;
                    top: 10px;
                    right: 10px;
                    text-align: center;
                    line-height: 24px;
                    z-index: 1;
                    cursor: pointer;

                    &:after {
                        content: '\2716';
                        font-size: 14px;
                        color: white;
                    }
                }
            }
        }

        .img-bg {
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            position: relative;
            padding-bottom: 100%;
        }

    </style>
    <div class="white_box mb_30">
        <div class="box_header ">
            <div class="main-title">
                <h3 class="mb-0">Add Multiple Product Image</h3>
            </div>
        </div>
        @include('admin.layouts.message')
        <form action="{{ route('admin.product.save_multi_pro_img') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <label><b>Product Name</b>: {{ $product->name }}</label>
                    <input id="file-input" name="product_image[]" class="form-control" type="file" multiple>
                    <input type="hidden" value="{{ $product->id }}" name="product_id">
                    <div id="preview"></div>
                </div>
                <div class="col-md-3">

                </div>
                <div class="col-md-4"></div>
                <div class="col-md-4 text-center mt-4">
                    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                    <a href="{{ route('admin.product.list') }}" class="btn btn-warning btn-sm">Go To List</a>
                </div>
                <div class="col-md-4"></div>
            </div>
        </form>
        <div class="mt-4">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Image</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($productImage as $key => $image)
                        <tr>
                            <th scope="row">{{ $key + 1 }}</th>
                            <td><img src="{{ productImagePath($image->image) }}" height="100" width="100" alt=""></td>
                            <td>
                                @if ($image->status == 1)
                                    Active
                                @else
                                    Inactive
                                @endif
                            </td>
                            <td>
                                @if ($image->status == 1)
                                    <a href="{{ route('admin.product.inactive_product_img', ['id' => $image->id]) }}"
                                        class="btn btn-danger btn-sm">Inactive</a>
                                @else
                                    <a href="{{ route('admin.product.active_product_img', ['id' => $image->id]) }}"
                                        class="btn btn-warning btn-sm">Active</a>
                                @endif

                                <a href="{{ route('admin.product.delete_product_img',['id'=>getEncrypt($image->id)]) }}" class="btn btn-danger btn-sm">Delete</a>

                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    @endsection
    @section('script')
        <script>
            function previewImages() {

                var $preview = $('#preview').empty();
                if (this.files) $.each(this.files, readAndPreview);

                function readAndPreview(i, file) {

                    if (!/\.(jpe?g|png|gif)$/i.test(file.name)) {
                        return alert(file.name + " is not an image");
                    } // else...

                    var reader = new FileReader();

                    $(reader).on("load", function() {
                        $preview.append($("<img/>", {
                            src: this.result,
                            height: 100
                        }));
                    });

                    reader.readAsDataURL(file);

                }

            }

            $('#file-input').on("change", previewImages);
        </script>
    @endsection
