@extends('admin.master')
@section('titles')
    Sub Category
@endsection
@section('admin_home')
    <div class="white_box mb_30">
        <div class="box_header ">
            <div class="main-title">
                <h3 class="mb-0">Update Sub Category</h3>
            </div>
        </div>
        @include('admin.layouts.message')
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <form action="{{ route('admin.subcategory.update') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" value="{{ $subcategory->name }}" name="name" class="form-control" />
                        <input type="hidden" value="{{ $subcategory->id }}" name="id" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Category Name</label>
                        <select type="text" name="category_id" class="form-control">
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}"
                                    {{ $subcategory->root_id == $category->id ? 'selected' : '' }}>{{ $category->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    {{-- <div class="form-group">
                        <label>Heading Name</label>
                        <input type="text" value="{{ $category->heading }}" class="form-control" name="heading" />
                    </div> --}}

                    <div class="form-group">
                        <label><b>Type</b> </label>
                        <br>
                        <div class="form-check form-check-inline">
                            <label>Sub category <input type="radio" {{ $subcategory->type == 1 ? 'checked' : '' }}
                                    name="type" value="1"></label>
                        </div>
                        <div class="form-check form-check-inline">
                            <label>Brand <input type="radio" {{ $subcategory->type == 2 ? 'checked' : '' }} name="type"
                                    value="2"></label>
                        </div>
                        <div class="form-check form-check-inline">
                            <label>Flavour<input type="radio" {{ $subcategory->type == 3 ? 'checked' : '' }} name="type"
                                    value="3"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Status</label>
                        <select name="status" class="form-control">
                            <option value="1" {{ $subcategory->status == 1 ? 'selected' : '' }}>Active</option>
                            <option value="2" {{ $subcategory->status == 2 ? 'selected' : '' }}>Inactive</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm">Update</button>
                    <a href="{{ route('admin.subcategory.list') }}" class="btn btn-info btn-sm">Go To List</a>
                </form>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
@endsection
