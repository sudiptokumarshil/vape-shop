<!DOCTYPE html>
<html lang="en">

<head>
    <title>Vape Fish Dubai</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/') }}client/assets/images/logo.png" />
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&amp;display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/animate.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/slick.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/chosen.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/magnific-popup.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/lightbox.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/js/fancybox/source/jquery.fancybox.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/jquery.scrollbar.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/mobile-menu.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/fonts/flaticon/flaticon.css">
    <link rel="stylesheet" href="{{ asset('/') }}client/assets/css/style.css">

<body>
    <div class="container mt-5 pt-5">
        <div class="main-content main-content-404 right-sidebar">
            <div class="container">
                <div class="row">
                    <div class="content-area content-404 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="site-main">
                            <section class="error-404 not-found">
                                <div class="images">
                                    <img src="{{ asset('/') }}client/assets/images/404.png" alt="img">
                                </div>
                                <div class="text-404">
                                    <h1 class="page-title">
                                        Error 404 Not Found
                                    </h1>
                                    <p class="page-content">
                                        We´re sorry but the page you are looking for does nor exist. <br />
                                        You could return to
                                        <a href="{{route('home')}}" class="hightlight"> Home page</a>
                                    </p>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
