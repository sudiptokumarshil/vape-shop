<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Email;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Traits\CommonTrait;
use App\Traits\EmailTrait;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\DB;

class CheckoutController extends Controller
{
    use CommonTrait;
    use EmailTrait;
    public function index()
    {
        $categories = $this->getCategory();
        return view('client.layouts.pages.checkout', [
            'categories' => $categories
        ]);
    }

    public function confirmOrder(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'address' => 'required',
        ]);

        if (cartNumber() == 0) {
            return redirect()->back()->with('error', 'Sorry. Your cart is empty!');
        }

        DB::beginTransaction();

        try {
            $order = new Order();
            $order->first_name = $request->first_name;
            $order->last_name = $request->last_name;
            $order->email = $request->email;
            $order->phone = $request->phone;
            $order->phone_alternative = $request->phone_alternative;
            $order->address = $request->address;
            $order->total = Cart::priceTotal();
            $order->save();

            $cartProduct = Cart::content();
            foreach ($cartProduct as $value) {
                $orderDetail = new OrderDetail();
                $orderDetail->order_id = $order->id;
                $orderDetail->product_id = $value->id;
                $orderDetail->price = $value->price;
                $orderDetail->qty = $value->qty;
                $orderDetail->sub_total = floatval($value->price * $value->qty);
                $orderDetail->save();
            }
            $this->sendEmail($order->id,"Congratulations! Your order is completed successfully.","Order Invoices");
            DB::commit();
            Cart::destroy();
            return redirect()->back()->with('success', 'Order Created Successfully!!');
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
            return redirect()->back()->with('error', 'Sorry. Something Went Wrong. Please Try Again After Sometime.');
        }
    }
}
