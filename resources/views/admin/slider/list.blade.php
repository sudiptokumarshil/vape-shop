@extends('admin.master')
@section('titles')
    Slider
@endsection
@section('admin_home')
    <div class="QA_section">
        <div class="white_box_tittle list_header">
            <h4>Sliders</h4>
            <div class="box_right d-flex lms_block">
                <div class="serach_field_2">

                </div>
                <div class="add_button ml-10">
                    <a href="{{ route('admin.slider.create') }}" class="btn_1">Add
                        Slider</a>
                </div>
            </div>
        </div>
        <div class="QA_table mb_30">
            <table class="table lms_table_active">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Title</th>
                        <th scope="col">Description</th>
                        <th scope="col">Image</th>
                        <th scope="col">Sort</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php($i = 1)
                    @foreach ($sliders as $key => $slider)
                        <tr>
                            <th scope="row"> {{ $i++ }}</th>
                            <td>{{ $slider->title }}</td>
                            <td>{{ $slider->description }}</td>
                            <td><img src="{{ sliderImagePath($slider->image) }}" style="height: 200" /></td>
                            <td>
                                <input type="number" value="{{ $slider->sorting }}"
                                    onkeyup="sortingSlider({{$slider->id}},{{ $key }})" style="width: 90px;"
                                    id="presentValue{{ $key }}" />
                            </td>
                            <td>
                                @if ($slider->status == 1)
                                    Active
                                @else
                                    Inactive
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('admin.slider.edit', ['id' => $slider->id]) }}" type="button"
                                    class="btn btn-warning">Edit</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="d-flex justify-content-center">
                {!! $sliders->links() !!}
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        const sortingSlider = (id,rowId) => {
            let PresentValue = $('#presentValue' + rowId).val();
            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
            });
            $.ajax({
                type: "POST",
                url: "/admin/slider/sorting",
                data: {
                    id: id,
                    value: PresentValue,
                },
                success: function(data) {

                },
            });
        }
    </script>
@endsection
