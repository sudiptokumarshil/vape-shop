<header class="header style2">
    <div class="top-bar">
        <div class="row" style="padding: 20px 0; text-align: center;">
            <div class="col-md-3">
                <p style="text-transform: uppercase; padding: 0; margin: 0;">free shipping above 500 aed shopping!</p>
            </div>
            <div class="col-md-6">
                <strong style="color: red;">Warning: These product contains nicotine. Nicotine is an addictive
                    chemicals.</strong>
            </div>
            <div class="col-md-3">
                Whatsapp: <a href="http://wa.me/+8801827298568"><strong>+88 01827-298 568</strong></a>
            </div>
        </div>
        <!-- <div class="top-bar-left">
                <div class="header-message">
                    Welcome to our online store!
                </div>
            </div>
            <div class="top-bar-right">
                <div class="header-language">
                    <div class="herald-language herald-dropdown">
                        <a href="#" class="active language-toggle" data-herald="herald-dropdown">
        <span>
         English (USD)
        </span>
                        </a>
                        <ul class="herald-submenu">
                            <li class="switcher-option">
                                <a href="#">
          <span>
           French (EUR)
          </span>
                                </a>
                            </li>
                            <li class="switcher-option">
                                <a href="#">
          <span>
           Japanese (JPY)
          </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <ul class="header-user-links">
                    <li>
                        <a href="login.html">Login or Register</a>
                    </li>
                </ul>
            </div> -->
    </div>
    <div class="container">
        <div class="main-header">
            <div class="row" style=" display: flex; align-items: center; justify-content: center;">
                <div class="col-lg-4 col-sm-6 col-md-4 col-xs-7 col-ts-12 header-element">
                    <div class="block-search-block">
                        <form class="form-search">
                            <div class="form-content">
                                <div class="inner">
                                    <input type="text" class="input search_key" name="search"
                                        placeholder="Search here">

                                    <span class="icon-search"></span>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-md-4 col-xs-5 col-ts-12">
                    <div class="logo">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset('/') }}client/assets/images/logo.png" alt="logo" style="width: 120px;">
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12 col-md-4 col-xs-12 col-ts-12">
                    <div class="header-control">
                        {{-- <div class="block-minicart herald-mini-cart block-header herald-dropdown"> --}}
                        <div class="block-minicart herald-mini-cart block-header">
                            <a href="{{ route('client.product.cart.index') }}" class="shopcart-icon">
                                Cart
                                <span class="count cart_qty">
                                    {{ cartNumber() }}
                                </span>
                            </a>
                            {{-- <div class="no-product herald-submenu">
                                <p class="text">
                                    You have
                                    <span>
                                        0 item(s)
                                    </span>
                                    in your bag
                                </p>
                            </div> --}}
                        </div>
                        {{-- </div> --}}
                        <a class="menu-bar mobile-navigation menu-toggle" href="#">
                            <span></span>
                            <span></span>
                            <span></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-nav-container">
        <div class="container">
            <div class="header-nav-wapper main-menu-wapper">
                <div class="header-nav">
                    <div class="container-wapper">
                        <ul class="herald-clone-mobile-menu herald-nav main-menu " id="menu-main-menu">
                            <li class="menu-item ">
                                <a href="{{ route('home') }}" class="herald-menu-item-title" title="Home">Home</a>
                            </li>
                            {{-- <li class="menu-item  menu-item-has-children item-megamenu">
                                <a href="#" class="herald-menu-item-title" title="Pages">Pages</a>
                                <span class="toggle-submenu"></span>
                                <div class="submenu mega-menu menu-page">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 menu-page-item">
                                            <div class="herald-custommenu default">
                                                <h2 class="widgettitle">Shop Pages</h2>
                                                <ul class="menu">
                                                    <li class="menu-item">
                                                        <a href="shoppingcart.html">Shopping Cart</a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="checkout.html">Checkout</a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="contact.html">Contact us</a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="404page.html">404</a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="login.html">Login/Register</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 menu-page-item">
                                            <div class="herald-custommenu default">
                                                <h2 class="widgettitle">Product</h2>
                                                <ul class="menu">
                                                    <li class="menu-item">
                                                        <a href="productdetails-fullwidth.html">Product
                                                            Fullwidth</a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="productdetails-leftsidebar.html">Product left
                                                            sidebar</a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="productdetails-rightsidebar.html">Product right
                                                            sidebar</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 menu-page-item">
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 menu-page-item">
                                        </div>
                                    </div>
                                </div>
                            </li> --}}
                            {{-- <li class="menu-item  menu-item-has-children item-megamenu">
                                <a href="#" class="herald-menu-item-title" title="Pages">Kits</a>
                                <span class="toggle-submenu"></span>
                                <div class="submenu mega-menu menu-page">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 menu-page-item">
                                            <div class="herald-custommenu default">
                                                <h2 class="widgettitle">By Brand</h2>
                                                <ul class="menu">
                                                    <li class="menu-item">
                                                        <a href="#">Sub Cat-1</a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="#">Sub Cat-2</a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="#">Sub Cat-3</a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="#">Sub Cat-4</a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="#">Sub Cat-5</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 menu-page-item">
                                            <div class="herald-custommenu default">
                                                <h2 class="widgettitle">By Flavour</h2>
                                                <ul class="menu">
                                                    <li class="menu-item">
                                                        <a href="#">Sub Cat-1</a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="#">Sub Cat-2</a>
                                                    </li>
                                                    <li class="menu-item">
                                                        <a href="#">Sub Cat-3</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li> --}}

                            @foreach ($categories as $category)
                                <li class="menu-item  menu-item-has-children item-megamenu">
                                    <a href="{{ route('client.product.category_product', ['id' => $category->id,'type'=>1]) }}"
                                        class="herald-menu-item-title" title="Pages">{{ $category->name }}</a>
                                    <span class="toggle-submenu"></span>
                                    <div class="submenu mega-menu menu-page">
                                        <div class="row">
                                            @if ($category->get_subcategory->count() > 0 && $category->get_brand->count() == 0 && $category->get_flovour->count() == 0)
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 menu-page-item">
                                                    <div class="herald-custommenu default">
                                                        {{-- <h2 class="widgettitle">By Sub Category</h2> --}}
                                                        <ul class="menu">
                                                            @foreach ($category->get_subcategory as $subcategory)
                                                                <li class="menu-item">
                                                                    <a
                                                                        href="{{ route('client.product.subcategory_product', ['id' => $subcategory->id,'type'=>2]) }}">{{ $subcategory->name }}</a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            @endif

                                            @if ($category->get_brand->count() > 0)
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 menu-page-item">
                                                    <div class="herald-custommenu default">
                                                        <h2 class="widgettitle">By Brand</h2>
                                                        <ul class="menu">
                                                            @foreach ($category->get_brand as $subcategory)
                                                                <li class="menu-item">
                                                                    <a
                                                                        href="{{ route('client.product.brand_product', ['id' => $subcategory->id,'type'=>3]) }}">{{ $subcategory->name }}</a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            @endif

                                            @if ($category->get_flovour->count() > 0)
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 menu-page-item">
                                                    <div class="herald-custommenu default">
                                                        <h2 class="widgettitle">By Flavour</h2>
                                                        <ul class="menu">
                                                            @foreach ($category->get_flovour as $subcategory)
                                                                <li class="menu-item">
                                                                    <a
                                                                        href="{{ route('client.product.flavour_product', ['id' => $subcategory->id,'type'=>4]) }}">{{ $subcategory->name }}</a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                </li>
                            @endforeach
                            {{-- <li class="menu-item  menu-item-has-children">
                                <a href="#" class="herald-menu-item-title" title="Category">Categories</a>
                                <span class="toggle-submenu"></span>
                                <ul class="submenu">
                                    @foreach ($categories as $category)
                                    <li class="menu-item menu-item-has-children">
                                        <a href="{{ route('client.product.category_product', ['id' => $category->id]) }}" class="herald-menu-item-title" title="Blog Style">{{ $category->name }}</a>
                                        <span class="toggle-submenu"></span>
                                        <ul class="submenu">
                                            @foreach ($category->get_subcategory as $subcategory)
                                            <li class="menu-item">
                                                <a href="{{ route('client.product.subcategory_product', ['id' => $subcategory->id]) }}">{{ $subcategory->name }}</a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    @endforeach
                                </ul>
                            </li> --}}

                            {{-- <li class="menu-item menu-item-has-children">
                                <a href="gridproducts.html" class="herald-menu-item-title" title="Brands">Brands</a>
                                <span class="toggle-submenu"></span>
                                <ul class="submenu">
                                    @foreach ($brands as $brand)
                                        <li class="menu-item">
                                            <a
                                                href="{{ route('client.product.brand_product', ['id' => $brand->id]) }}">{{ $brand->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                            {{-- <li class="menu-item">
                                <a href="about.html" class="herald-menu-item-title" title="About">About</a>
                            </li> --}}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="header-device-mobile">
    <div class="wapper">
        <div class="item mobile-logo">
            <div class="logo">
                <a href="{{ route('home') }}">
                    <img src="{{ asset('/') }}client/assets/images/logo.png" alt="img">
                </a>
            </div>
        </div>
        <div class="item item mobile-search-box has-sub">
            <a href="#">
                <span class="icon">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </span>
            </a>
            <div class="block-sub">
                <a href="#" class="close">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a>
                <div class="header-searchform-box">
                    <form class="header-searchform">
                        <div class="searchform-wrap">
                            <input type="text" class="search-input search_key" placeholder="Enter keywords to search...">
                            {{-- <input type="submit" class="submit button" value="Search"> --}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{-- <div class="item mobile-settings-box has-sub">
            <a href="#">
                <span class="icon">
                    <i class="fa fa-cog" aria-hidden="true"></i>
                </span>
            </a>
            <div class="block-sub">
                <a href="#" class="close">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a>
                <div class="block-sub-item">
                    <h5 class="block-item-title">Currency</h5>
                    <form class="currency-form herald-language">
                        <ul class="herald-language-wrap">
                            <li class="active">
                                <a href="#">
                                    <span>
                                        English (USD)
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>
                                        French (EUR)
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span>
                                        Japanese (JPY)
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div> --}}
        <div class="item menu-bar">
            <a class=" mobile-navigation  menu-toggle" href="#">
                <span></span>
                <span></span>
                <span></span>
            </a>
        </div>
    </div>
</div>
<a href="http://wa.me/+8801827298568" class="whatsapp" target="_blank">
    <i class="fa fa-whatsapp" aria-hidden="true" style="font-size: 40px;"></i>
</a>
