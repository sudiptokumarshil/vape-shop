<?php

namespace App\Traits;

use App\Events\Notify;
use Pusher\Pusher;

trait NotifyTrait
{
    public static function notify($data)
    {
        $options = array(
            'cluster' => env('PUSHER_APP_CLUSTER'),
            'encrypted' => true
        );
        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );

        $pusher->trigger('sent-notification', Notify::class, $data);
    }

    public static function notifyMessage($message)
    {
        $options = array(
            'cluster' => env('PUSHER_APP_CLUSTER'),
            'encrypted' => true
        );
        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );
        $data['message'] = $message;
        $pusher->trigger('sent-notification', Notify::class, $data);
    }
}
