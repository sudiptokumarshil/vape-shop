<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function get_category()
    {
        return $this->belongsTo(self::class, 'root_id', 'id');
    }
    public function get_subcategory()
    {
        return $this->hasMany(self::class, 'root_id', 'id')->where('type',1);
    }
    public function get_brand()
    {
        return $this->hasMany(self::class, 'root_id', 'id')->where('type',2);
    }
    public function get_flovour()
    {
        return $this->hasMany(self::class, 'root_id', 'id')->where('type',3);
    }
}
