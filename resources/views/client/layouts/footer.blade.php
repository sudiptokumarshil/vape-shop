<footer class="footer style7">
    <div class="container">
        <div class="container-wapper">
            <div class="row">
                <div class="box-footer col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <div class="herald-custommenu default">
                        <h2 class="widgettitle">Company</h2>
                        <ul class="menu">
                            <li class="menu-item">
                                <a href="#">About Us</a>
                            </li>
                            <li class="menu-item">
                                <a href="{{ route('contact') }}">Contact Us</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="box-footer col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <h2 class="widgettitle">Legal</h2>
                    <ul class="menu">
                        <li class="menu-item">
                            <a href="{{ route('privacy') }}">Privacy Policy</a>
                        </li>
                        <li class="menu-item">
                            <a href="{{ route('payment') }}">Payment Policy</a>
                        </li>
                        <li class="menu-item">
                            <a href="{{ route('shipping') }}">Shipping Policy</a>
                        </li>
                        <li class="menu-item">
                            <a href="#">Terms & Conditions</a>
                        </li>
                    </ul>
                </div>
                <div class="box-footer col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    {{-- <div class="herald-newsletter style1">
                        <div class="newsletter-head">
                            <h3 class="title">Newsletter</h3>
                        </div>
                        <div class="newsletter-form-wrap">
                            <div class="list">
                                Join up to get the latest on sales, new arrivals and more...
                            </div>
                            <input type="email" class="input-text email email-newsletter"
                                placeholder="Your email letter">
                            <button class="button btn-submit submit-newsletter">SUBSCRIBE</button>
                        </div>
                    </div> --}}

                    <h2 class="widgettitle">Service Point</h2>
                    <ul class="menu">
                        <li class="menu-item">
                            <a href="http://wa.me/+8801827298568">+88 01827-298 568</a>
                        </li>
                        <li class="menu-item">
                            <a href="mail:info@vapefishdubai.com">info@vapefishdubai.com</a>
                        </li>
                    </ul>
                </div>
                <div class="box-footer col-xs-8 col-sm-8 col-md-3 col-lg-3">
                   
                    <h2 class="widgettitle">Social</h2>
                    <div class="row">
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 social-icon">
                            <a href="https://www.facebook.com/vapefishdubai/" target="_blank">
                                <img src="{{ asset('/') }}client/assets/images/facebook.png" alt="" class="img-fluid">
                            </a>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 social-icon">
                            <a href="https://www.instagram.com/vape_fish_/" target="_blank">
                                <img src="{{ asset('/') }}client/assets/images/instagram.png" alt="" class="img-fluid">
                            </a>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 social-icon">
                            <a href="#" target="_blank">
                                <img src="{{ asset('/') }}client/assets/images/twitter.png" alt="" class="img-fluid">
                            </a>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 social-icon">
                            <a href="http://api.whatsapp.com/send?phone=971556936716" target="_blank">
                                <img src="{{ asset('/') }}client/assets/images/whatsapp.png" alt="" class="img-fluid">
                            </a>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 social-icon">
                            <a href="#" target="_blank">
                                <img src="{{ asset('/') }}client/assets/images/google.png" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 border-custom">
                    <span></span>
                </div>
            </div>
            <div class="footer-end">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        {{-- <div class="herald-payment">
                            <img src="{{asset('/')}}client/assets/images/payments.png" alt="img">
                    </div> --}}
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="coppyright">
                        Copyright © <?php echo date('Y'); ?>
                        <a href="#"> Vape Fish Dubai</a>
                        . All rights reserved
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</footer>
<div class="footer-device-mobile">
    <div class="wapper">
        <div class="footer-device-mobile-item device-home">
            <a href="{{ route('home') }}">
                <span class="icon">
                    <i class="fa fa-home" aria-hidden="true"></i>
                </span>
                Home
            </a>
        </div>
        {{-- <div class="footer-device-mobile-item device-home device-wishlist">
            <a href="#">
                <span class="icon">
                    <i class="fa fa-heart" aria-hidden="true"></i>
                </span>
                Wishlist
            </a>
        </div> --}}
        <div class="footer-device-mobile-item device-home device-cart">
            <a href="{{route('client.product.cart.index')}}">
                <span class="icon cart_qty">
                    <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                    <span class="count-icon">
                        {{ cartNumber() }}
                    </span>
                </span>
                <span class="text">Cart</span>
            </a>
        </div>
        {{-- <div class="footer-device-mobile-item device-home device-user">
            <a href="login.html">
                <span class="icon">
                    <i class="fa fa-user" aria-hidden="true"></i>
                </span>
                Account
            </a>
        </div> --}}
    </div>
</div>
<a href="#" class="backtotop">
    <i class="fa fa-chevron-up"></i>
</a>
