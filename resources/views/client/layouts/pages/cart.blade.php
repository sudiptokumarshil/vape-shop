@extends('client.master')
@section('client_title')
Shopping Cart
@endsection
@section('home')
<div class="site-content">
    <main class="site-main  main-container no-sidebar">
        <div class="container">
            <div class="breadcrumb-trail breadcrumbs">
                <ul class="trail-items breadcrumb">
                    <li class="trail-item trail-begin">
                        <a href="{{route('home')}}">
                            <span>
                                Home
                            </span>
                        </a>
                    </li>
                    <li class="trail-item trail-end active">
                        <span>
                            Shopping Cart
                        </span>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="main-content-cart main-content col-sm-12">
                    <h3 class="custom_blog_title">
                        Shopping Cart
                    </h3>
                    <div class="page-main-content">
                        <div class="shoppingcart-content">
                            <form action="https://ledthanhdat.vn/html/herald/shoppingcart.html" class="cart-form">
                                <table class="shop_table">
                                    <thead>
                                        <tr>
                                            <th class="product-remove"></th>
                                            <th class="product-thumbnail"></th>
                                            <th class="product-name"></th>
                                            <th class="product-price"></th>
                                            <th class="product-quantity"></th>
                                            <th class="product-subtotal"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($carts as $cart)
                                        <tr class="cart_item tableRow-{{ $cart->rowId }}">
                                            <td class="product-remove">
                                                <a data-id="{{ $cart->rowId }}" onclick="cartRemove(this)" class="remove"></a>
                                            </td>
                                            <td class="product-thumbnail">
                                                <a href="#">
                                                    <img src="{{ productImagePath($cart->options->image) }}" alt="img" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image">
                                                </a>
                                            </td>
                                            <td class="product-name" data-title="Product">
                                                <a href="{{ route('client.product.details', ['slug' => $cart->options->url]) }}" class="title">{{ $cart->name }}</a>
                                                {{-- <span class="attributes-select attributes-color">Black,</span>
                                                        <span class="attributes-select attributes-size">XXL</span> --}}
                                            </td>
                                            <td class="product-quantity" data-title="Quantity">
                                                <div class="quantity">
                                                    <div class="control">
                                                        <a class="btn-number qtyminus quantity-minus" data-id="{{ $cart->rowId }}" onclick="decreaseQty(this)" href="#">-</a>
                                                        <input type="text" readonly value="{{ $cart->qty }}" data-step="1" data-min="0" value="1" title="Qty" class="input-qty qty-{{ $cart->rowId }}"
                                                               size="4">
                                                        <a href="#" onclick="increaseQty(this)" data-id="{{ $cart->rowId }}" class="btn-number qtyplus quantity-plus">+</a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="product-price" data-title="Price">
                                                <span class="woocommerce-Price-amount amount">
                                                    <span class="woocommerce-Price-currencySymbol">
                                                        AED <p class="totalPrice-{{ $cart->rowId }}">
                                                            {{ $cart->price * $cart->qty }}</p>
                                                    </span>

                                                </span>
                                            </td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <td class="actions">
                                                <div class="coupon">
                                                    {{-- <label class="coupon_code">Coupon Code:</label>
                                                        <input type="text" class="input-text"
                                                            placeholder="Promotion code here">
                                                        <a href="#" class="button"></a> --}}
                                                </div>
                                                <div class="order-total">
                                                    <span class="title">
                                                        Total Price:
                                                    </span>
                                                    <span class="total-price final_amount_before">
                                                        AED {{ $total }}
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </form>
                            <div class="control-cart">
                                <a href="{{ route('home') }}" class="button btn-continue-shopping">
                                    Continue Shopping
                                </a>
                                @if (cartNumber() > 0)
                                <a href="{{ route('client.product.checkout.index') }}" class="button btn-cart-to-checkout">
                                    Checkout
                                </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
@endsection
