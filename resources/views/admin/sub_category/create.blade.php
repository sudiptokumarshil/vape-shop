@extends('admin.master')
@section('titles')
    Sub Category
@endsection
@section('admin_home')
    <div class="white_box mb_30">
        <div class="box_header ">
            <div class="main-title">
                <h3 class="mb-0">Create Sub Category</h3>
            </div>
        </div>
        @include('admin.layouts.message')
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <form action="{{ route('admin.subcategory.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" name="name" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Category Name</label>
                        <select type="text" name="category_id" class="form-control">
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    {{-- <div class="form-group">
                        <label>Heading Name</label>
                        <input type="text" class="form-control" name="heading"/>
                    </div> --}}
                    <div class="form-group">
                        <label><b>Type</b> </label>
                        <br>
                        <div class="form-check form-check-inline">
                            <label>Sub category <input type="radio" checked name="type" value="1"></label>
                        </div>
                        <div class="form-check form-check-inline">
                            <label>Brand <input type="radio" name="type" value="2"></label>
                        </div>
                        <div class="form-check form-check-inline">
                            <label>Flavour<input type="radio" name="type" value="3"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Status</label>
                        <select name="status" class="form-control">
                            <option value="1">Active</option>
                            <option value="2">Inactive</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm">Save</button>
                    <a href="{{ route('admin.subcategory.list') }}" class="btn btn-info btn-sm">Go To List</a>
                </form>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
@endsection
