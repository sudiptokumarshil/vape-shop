<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    public function pendingOrder()
    {
        $order = Order::where('status', 1)->orderBy('id', 'desc')->paginate(30);
        return view('admin.order.pending_order', [
            'orders' => $order
        ]);
    }

    public function acceptOrder($id)
    {
        $accept = Order::find($id);
        $accept->status = 2;
        $accept->update();
        return redirect()->route('admin.order.accepted')->with('success', 'Order Proccesing Successfully!');
    }
    public function cancelOrder($id)
    {
        $accept = Order::find($id);
        $accept->status = 3;
        $accept->update();
        return redirect()->back()->with('success', 'Order Cancelled Successfully!');
    }

    public function acceptedOrder()
    {
        $order = Order::where('status', 2)->orderBy('id', 'desc')->paginate(30);
        return view('admin.order.accepted_order', [
            'orders' => $order
        ]);
    }
    public function cancellededOrder()
    {
        $order = Order::where('status', 3)->orderBy('id', 'desc')->paginate(30);
        return view('admin.order.cancelleded_order', [
            'orders' => $order
        ]);
    }

    public function deliveredOrder()
    {
        $order = Order::where('status', 4)->orderBy('id', 'desc')->paginate(30);
        return view('admin.order.delivered_order', [
            'orders' => $order
        ]);
    }
    public function deliverOrder($id)
    {
        $accept = Order::find($id);
        $accept->status = 4;
        $accept->update();
        return redirect()->route('admin.order.delivered')->with('success', 'Order Delivered Successfully!');
    }

    public function invoice($id)
    {
        $invoice = Order::with(['invoiceDetails'])->find($id);
        return view('admin.order.invoice', [
            'invoice' => $invoice
        ]);
    }
}
