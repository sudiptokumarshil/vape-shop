@extends('admin.master')
@section('titles')
    Processing Order
@endsection
@section('admin_home')
    <div class="QA_section">
        <div class="white_box_tittle list_header">
            <h4>Processing Order</h4>
        </div>
        <div class="QA_table mb_30">
            <table class="table lms_table_active cls">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Date</th>
                        <th scope="col">Customer Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Phone Alternative</th>
                        <th scope="col">Address</th>
                        <th scope="col">Total</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $key => $order)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ formatedDateForDisplay($order->created_at) }}</td>
                            <td>{{ $order->first_name . ' ' . $order->last_name }}</td>
                            <td>{{ $order->email }}</td>
                            <td>{{ $order->phone }}</td>
                            <td>{{ $order->phone_alternative }}</td>
                            <td>{{ $order->address }}</td>
                            <td>{{ $order->total }}</td>
                            <td>
                                <a href="{{ route('admin.order.invoice', ['id' => $order->id]) }}"
                                    target="_blank" class="btn btn-success btn-sm">Details</a>
                                ||
                                @if ($order->status == 2)
                                    <a href="{{ route('admin.order.cancel', ['id' => $order->id]) }}"
                                        class="btn btn-danger btn-sm">Cancel</a>

                                    <a href="{{ route('admin.order.deliver', ['id' => $order->id]) }}"
                                        class="btn btn-primary btn-sm">Delivered</a>
                                @endif

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="d-flex justify-content-center">
                {{ $orders->links() }}
            </div>
        </div>
    </div>
@endsection
