@extends('client.master')
@section('client_title', $product->title)
@section('client_meta', $product->meta)
@section('home')
    <div class="main-content main-content-details single no-sidebar">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-trail breadcrumbs">
                        <ul class="trail-items breadcrumb">
                            {{-- <li class="trail-item trail-begin">
                            <a href="index.html">Home</a>
                        </li>
                        <li class="trail-item">
                            <a href="#">Creamy</a>
                        </li>
                        <li class="trail-item trail-end active">
                            Ehpro Cold Steel
                        </li> --}}
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-area content-details full-width col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <div class="site-main">
                        <div class="details-product">
                            <div class="details-thumd">
                                <div class="image-preview-container image-thick-box image_preview_container" id="main_img">
                                    <img id="img_zoom" data-zoom-image="{{ productImagePath($product->image) }}"
                                        src="{{ productImagePath($product->image) }}" alt="img">
                                    <a href="#" class="btn-zoom open_qv"></a>
                                </div>
                                <div class="product-preview image-small product_preview">
                                    <div id="thumbnails" class="thumbnails_carousel owl-carousel" data-nav="true"
                                        data-autoplay="false" data-dots="false" data-loop="false" data-margin="10"
                                        data-responsive='{"0":{"items":3},"480":{"items":3},"600":{"items":3},"1000":{"items":3}}'>

                                        <a href="#" data-image="{{ productImagePath($product->image) }}"
                                            data-zoom-image="{{ productImagePath($product->image) }}">
                                            <img src="{{ productImagePath($product->image) }}"
                                                data-large-image="{{ productImagePath($product->image) }}" alt="img">
                                        </a>

                                        @foreach ($product->get_product_image as $key => $proImg)
                                            <a href="#" data-image="{{ productImagePath($proImg->image) }}"
                                                data-zoom-image="{{ productImagePath($proImg->image) }}"
                                                @if ($key == 0) class="active" @endif>
                                                <img src="{{ productImagePath($proImg->image) }}"
                                                    data-large-image="{{ productImagePath($proImg->image) }}"
                                                    alt="img">
                                            </a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="details-infor">
                                <h1 class="product-title">
                                    {{ $product->name }}
                                </h1>
                                {{-- <div class="stars-rating">
                                <div class="star-rating">
                                    <span class="star-5"></span>
                                </div>
                                <div class="count-star">
                                    (7)
                                </div>
                            </div> --}}
                                <div class="availability">
                                    availability:
                                    {!! checkStockAvailable($product->is_stock) !!}
                                </div>
                                <div class="price">
                                    <span>AED {{ getPriceAfterDiscount($product->price, $product->discount) }}</span>
                                </div>
                                <div class="product-details-description">
                                    <ul>
                                        <li style="list-style: none">{!! $product->short_description !!}</li>

                                    </ul>
                                </div>
                                <div class="variations">
                                    <div class="attribute attribute_color">
                                        <div class="size-text text-attribute">
                                            Colors:
                                        </div>
                                        <div class="list-size list-item">
                                            <select title="size" name="color_image" id="product-color"
                                                onchange="getColorImage(this.value)">
                                                <option value="">Colors</option>
                                                @foreach ($product->get_color as $color)
                                                    <option value="{{ $color->id }}">{{ $color->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- <div class="attribute attribute_size">
                                        <div class="size-text text-attribute">
                                            Bottle Size:
                                        </div>
                                        <div class="list-size list-item">
                                            <select title="size">
                                                <option value="10ml">10ml</option>
                                                <option value="30ml">30ml</option>
                                                <option value="50ml">50ml</option>
                                                <option value="60ml">60ml</option>
                                                <option value="80ml">80ml</option>
                                                <option value="100ml">100ml</option>
                                            </select>
                                        </div>
                                    </div> --}}
                                </div>
                                <div class="group-button">
                                    {{-- <div class="yith-wcwl-add-to-wishlist">
                                    <div class="yith-wcwl-add-button">
                                        <a href="#">Add to Wishlist</a>
                                    </div>
                                </div>
                                <div class="size-chart-wrapp">
                                    <div class="btn-size-chart">
                                        <a id="size_chart" href="assets/images/size-chart.jpg" class="fancybox">View
                                            Size Chart</a>
                                    </div>
                                </div> --}}
                                    <div class="quantity-add-to-cart">
                                        {{-- <div class="quantity">
                                            <div class="control">
                                                <a class="btn-number qtyminus quantity-minus" href="#">-</a>
                                                <input type="text" data-step="1" data-min="0" value="1" title="Qty"
                                                    class="input-qty qty" size="4">
                                                <a href="#" class="btn-number qtyplus quantity-plus">+</a>
                                            </div>
                                        </div> --}}
                                        <button id="cartbtn-{{ $product->id }}" class="single_add_to_cart_button button"
                                            onclick="addToCart({{ $product->id }})">Add to cart
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-details-product">
                            <ul class="tab-link">
                                <li class="active">
                                    <a data-toggle="tab" aria-expanded="true" href="#product-descriptions">Descriptions </a>
                                </li>
                                {{-- <li class="">
                                <a data-toggle="tab" aria-expanded="true" href="#information">Information </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" aria-expanded="true" href="#reviews">Reviews</a>
                            </li> --}}
                            </ul>
                            <div class="tab-container">
                                <div id="product-descriptions" class="tab-panel active">
                                    <p>
                                        {!! $product->description !!}
                                    </p>
                                </div>

                            </div>
                        </div>
                        <div style="clear: left;"></div>
                        <div class="related products product-grid">
                            <h2 class="product-grid-title">You may also like</h2>
                            <div class="owl-products owl-slick equal-container nav-center"
                                data-slick='{"autoplay":false, "autoplaySpeed":1000, "arrows":true, "dots":false, "infinite":true, "speed":800, "rows":1}'
                                data-responsive='[{"breakpoint":"2000","settings":{"slidesToShow":3}},{"breakpoint":"1200","settings":{"slidesToShow":2}},{"breakpoint":"992","settings":{"slidesToShow":2}},{"breakpoint":"480","settings":{"slidesToShow":1}}]'>
                                @foreach ($relatedProducts as $relatedProduct)
                                    <div class="product-item style-1">
                                        <div class="product-inner equal-element">
                                            <div class="product-top">
                                                <div class="flash">
                                                    <span class="onnew">
                                                        <span class="text">
                                                            new
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="product-thumb">
                                                <div class="thumb-inner">
                                                    <a
                                                        href="{{ route('client.product.details', ['slug' => $relatedProduct->url]) }}">
                                                        <img src="{{ productImagePath($relatedProduct->image) }}"
                                                            alt="img">
                                                    </a>
                                                    <div class="thumb-group">
                                                        {{-- <div class="yith-wcwl-add-to-wishlist">
                                                            <div class="yith-wcwl-add-button">
                                                                <a href="#">Add to Wishlist</a>
                                                            </div>
                                                        </div> --}}
                                                        {{-- <a href="#" class="button quick-wiew-button">Quick View</a> --}}
                                                        <div class="loop-form-add-to-cart">
                                                            <button id="cartbtn-{{ $relatedProduct->id }}"
                                                                class="single_add_to_cart_button button"
                                                                onclick="addToCart({{ $relatedProduct->id }})">Add to
                                                                cart
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-info">
                                                <h5 class="product-name product_title">
                                                    <a
                                                        href="{{ route('client.product.details', ['slug' => $relatedProduct->url]) }}">{{ $relatedProduct->name }}</a>
                                                </h5>
                                                <div class="group-info">
                                                    {{-- <div class="stars-rating">
                                                        <div class="star-rating">
                                                            <span class="star-3"></span>
                                                        </div>
                                                        <div class="count-star">
                                                            (3)
                                                        </div>
                                                    </div> --}}
                                                    <div class="price">
                                                        @if (!empty($relatedProduct->discount))
                                                            <del>
                                                                AED {{ $relatedProduct->price }}
                                                            </del>
                                                            <ins>
                                                                AED
                                                                {{ getPriceAfterDiscount($relatedProduct->price, $relatedProduct->discount) }}
                                                            </ins>
                                                        @else
                                                            <ins>
                                                                AED {{ $relatedProduct->price }}
                                                            </ins>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('front_script')
    <script>
        function getColorImage(id) {
            $.ajax({
                url: "/get-color-image/" + id,
                type: "GET",
                data: {
                    _token: "{{ csrf_token() }}",
                },
                success: function(data) {
                    if (typeof data !== 'undefined') {
                       let dataHtml =`<div class="image-preview-container image-thick-box image_preview_container" id="main_img">
                            <img id="img_zoom" data-zoom-image="/admin/product/${data.color_image}" src="/admin/product/${data.color_image}" alt="img">
                            <a href="#" class="btn-zoom open_qv"></a>
                            </div>
                            `;
                        $('#main_img').html(dataHtml);
                    }

                },
            });
        }
    </script>
@endsection
