$(document).on("keyup", ".search_key", function () {
    let vv = $(this);
    let key = $(this).val();
    const link = getUrl();
    const method = "POST";
    const url = link + "search";
    const data = {
        query: key,
    };
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    ajaxSetup(
        function (data) {
            $(vv).autocomplete({
                source: data,
                select: function (e, ui) {
                    let id = ui.item.url;
                    window.location = link + "product/details/"+id;
                },
            });
        },
        method,
        url,
        data
    );
});
