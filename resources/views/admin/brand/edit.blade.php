@extends('admin.master')
@section('titles')
    Brand
@endsection
@section('admin_home')
    <div class="white_box mb_30">
        <div class="box_header ">
            <div class="main-title">
                <h3 class="mb-0">Update Brand</h3>
            </div>
        </div>
        @include('admin.layouts.message')
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <form action="{{ route('admin.brand.update') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" value="{{ $brand->name }}" name="name" class="form-control" />
                        <input type="hidden" value="{{ $brand->id }}" name="id" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select name="status" class="form-control">
                            <option value="1" {{ $brand->status == 1 ? 'selected' : '' }}>Active</option>
                            <option value="2" {{ $brand->status == 2 ? 'selected' : '' }}>Inactive</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm">Update</button>
                    <a href="{{ route('admin.brand.list') }}" class="btn btn-info btn-sm">Go To List</a>
                </form>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
@endsection
