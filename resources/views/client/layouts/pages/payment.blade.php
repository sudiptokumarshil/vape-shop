@extends('client.master')
@section('client_title')
Payment Information
@endsection
@section('home')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumb-trail breadcrumbs">
                <ul class="trail-items breadcrumb">
                    <li class="trail-item trail-begin">
                        <a href="index.html">Home</a>
                    </li>
                    <li class="trail-item trail-end active">
                        Payment Policy
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="content-area content-contact col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="site-main">
                <h3 class="custom_blog_title">Payment Policy</h3>
            </div>
        </div>
    </div>
</div>
<div class="page-main-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="main-info-post">
                    <p>
                        To begin purchasing from the site or platform, you may be required to register using a valid phone number and password. It is your sole responsibility to keep such information/log-in details secure. We shall not be held responsible if such information/log-in details are provided to a third party by the customer or if any third party receives the same due to the negligence of the customer and for any loss incurred as a result of the above-mentioned incidents (unauthorized access to data).
                    </p>
                    <p>
                        We only accept cash on delivery at the moment.
                    </p>
                    <p>
                        <b>Cash on delivery:</b> We deliver our product all over UAE, our delivery agent will collect the cash after delivering the ordered product(s). 
                    </p>
                    <h3 class="post-title  ">
                        <a href="#">Are there any International Payment methods?</a>
                    </h3>
                    <p>
                        We don't have any online payment method yet. We will add online payment method in the future.
                    </p>
                    <h3 class="post-title  ">
                        <a href="#">Are there any hidden charges?</a>
                    </h3>
                    <p>
                        No, there are no hidden charges. All payment information will be shown on the checkout page. Payment will include delivery charges and VAT accordingly. You will only pay the amount shown on the checkout page.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
