<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Slider;
use App\Traits\CommonTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    use CommonTrait;
    public function index()
    {
        $categories = $this->getCategory(); //NOTE: FOR GETTING CATEGORIES ...
        $sliders = Slider::select('id', 'title', 'description', 'status', 'image')
            ->where('status', 1)
            ->orderBy('sorting', 'asc')
            ->limit(8)
            ->get();

        $products = Product::select('id', 'name', 'url', 'category_id', 'subcategory_id', 'brand_id', 'price', 'discount', 'image', 'is_stock')
            ->where('status', 1)
            ->inRandomOrder()
            ->limit(8)
            ->get();

        $disposable = Product::select('id', 'name', 'url', 'category_id', 'subcategory_id', 'brand_id', 'price', 'discount', 'image', 'is_stock')
            ->where('status', 1)
            ->where('category_id', 11)
            ->take(8)
            ->orderBy('id', 'desc')
            ->get(); //id = 11 in live database ... (Categories Table)

        $juice = Product::select('id', 'name', 'url', 'category_id', 'subcategory_id', 'brand_id', 'price', 'discount', 'image', 'is_stock')
            ->where('status', 1)
            ->where('category_id', 5)
            ->take(8)
            ->orderBy('id', 'desc')
            ->get(); //id = 5 in live database ... (Categories Table)

        $weeklyProducts = Product::select('id', 'name', 'url', 'category_id', 'subcategory_id', 'brand_id', 'price', 'discount', 'image', 'is_stock')
            ->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
            ->where('status', 1)
            ->take(20)
            ->orderBy('id', 'desc')
            ->get();

        return view('client.layouts.pages.home', compact('products', 'sliders', 'disposable', 'juice', 'weeklyProducts', 'categories', 'categories'));
    }

    // NOTE: FOR SEARCHING PRODUCT ...
    public function search(Request $request)
    {
        $data = array();
        if ($request->get('query')) {
            $query = $request->get('query');
            $result = DB::table('products')->select('id', 'name', 'status', 'url')
                ->where('status', 1)
                ->where('name', 'LIKE', "%{$query}%")
                ->take(50)
                ->get();


            foreach ($result as $val) {
                $ara = array();
                $ara["id"] = $val->id;
                $ara["url"] = $val->url;
                $ara["value"] = $val->name;
                array_push($data, $ara);
            }
        }
        return response()->json($data);
    }
}
