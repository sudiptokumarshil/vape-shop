<!DOCTYPE html>
<html lang="zxx">

<!-- Mirrored from demo.dashboardpack.com/hospital-html/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 07 Mar 2022 15:18:11 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('titles')</title>

    <link rel="stylesheet" href="{{ asset('/') }}admin/css/bootstrap.min.css" />

    <link rel="stylesheet" href="{{ asset('/') }}admin/vendors/themefy_icon/themify-icons.css" />

    <link rel="stylesheet" href="{{ asset('/') }}admin/vendors/swiper_slider/css/swiper.min.css" />

    <link rel="stylesheet" href="{{ asset('/') }}admin/vendors/select2/css/select2.min.css" />

    <link rel="stylesheet" href="{{ asset('/') }}admin/vendors/niceselect/css/nice-select.css" />

    <link rel="stylesheet" href="{{ asset('/') }}admin/vendors/owl_carousel/css/owl.carousel.css" />

    <link rel="stylesheet" href="{{ asset('/') }}admin/vendors/gijgo/gijgo.min.css" />

    <link rel="stylesheet" href="{{ asset('/') }}admin/vendors/font_awesome/css/all.min.css" />
    <link rel="stylesheet" href="{{ asset('/') }}admin/vendors/tagsinput/tagsinput.css" />
    <link rel="stylesheet" href="{{ asset('/') }}admin/vendors/text_editor/summernote-bs4.css" />
    <link rel="stylesheet" href="{{ asset('/') }}admin/vendors/morris/morris.css">
    <link rel="stylesheet" href="{{ asset('/') }}admin/vendors/material_icon/material-icons.css" />
    <link rel="stylesheet" href="{{ asset('/') }}admin/css/metisMenu.css">
    <link rel="stylesheet" href="{{ asset('/') }}admin/css/style.css" />
    <link rel="stylesheet" href="{{ asset('/') }}admin/css/colors/default.css" id="colorSkinCSS">
    <style>
        .notification {
            position: relative;
            text-align: right;
        }

        .notification img {
            padding: 10px 0;
        }

        .notification-menu {
            position: absolute;
            top: 50px;
            left: -150px;
            background-color: #e5e5e5;
            border: #989898;
            padding: 10px;
            list-style: none;
            width: 250px;
            display: none;
            text-align: left;
        }

        .notification-menu li {
            background-color: #fff;
            padding: 10px;
            margin-bottom: 10px;
        }

        .notification-menu .avatar {
            padding: 2px 0;
        }

        .notification-menu h3 {
            font-size: 15px;
            margin: 0 0 5px 0;
            display: inline;
        }

        .notification-menu p {
            margin-bottom: 0;
            font-size: 14px;
        }
    </style>
</head>

<body class="crm_body_bg">

    @include('admin.layouts.sidebar')
    <section class="main_content dashboard_part">

        @include('admin.layouts.header')

        <div class="main_content_iner ">
            <div class="container-fluid p-0">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        @yield('admin_home')
                    </div>
                </div>
            </div>
        </div>

        @include('admin.layouts.footer')
    </section>



    <script src="{{ asset('/') }}admin/js/jquery-3.4.1.min.js"></script>

    <script src="{{ asset('/') }}admin/js/popper.min.js"></script>

    <script src="{{ asset('/') }}admin/js/bootstrap.min.js"></script>

    <script src="{{ asset('/') }}admin/js/metisMenu.js"></script>

    <script src="{{ asset('/') }}admin/vendors/count_up/jquery.waypoints.min.js"></script>

    <script src="{{ asset('/') }}admin/vendors/chartlist/Chart.min.js"></script>

    <script src="{{ asset('/') }}admin/vendors/count_up/jquery.counterup.min.js"></script>

    <script src="{{ asset('/') }}admin/vendors/swiper_slider/js/swiper.min.js"></script>

    <script src="{{ asset('/') }}admin/vendors/niceselect/js/jquery.nice-select.min.js"></script>

    <script src="{{ asset('/') }}admin/vendors/owl_carousel/js/owl.carousel.min.js"></script>

    <script src="{{ asset('/') }}admin/vendors/gijgo/gijgo.min.js"></script>

    <script src="{{ asset('/') }}admin/vendors/datatable/js/buttons.flash.min.js"></script>
    <script src="{{ asset('/') }}admin/vendors/datatable/js/jszip.min.js"></script>
    <script src="{{ asset('/') }}admin/vendors/datatable/js/pdfmake.min.js"></script>
    <script src="{{ asset('/') }}admin/vendors/datatable/js/vfs_fonts.js"></script>
    <script src="{{ asset('/') }}admin/vendors/datatable/js/buttons.html5.min.js"></script>
    <script src="{{ asset('/') }}admin/vendors/datatable/js/buttons.print.min.js"></script>
    <script src="{{ asset('/') }}admin/vendors/progressbar/jquery.barfiller.js"></script>
    <script src="{{ asset('/') }}admin/vendors/tagsinput/tagsinput.js"></script>
    <script src="{{ asset('/') }}admin/vendors/text_editor/summernote-bs4.js"></script>
    <script src="{{ asset('/') }}admin/js/custom.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.select_2').select2();
            main();
        });

        function main() {
            $('.notification img').click(function() {
                $('.notification-menu').toggle();
            });

            $('.post .btn').click(function() {
                $(this).toggleClass('btn-like');
            });
        };
    </script>
    @yield('script')

    <script src="https://js.pusher.com/7.2/pusher.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script>
        let pusher = new Pusher('{{ env('MIX_PUSHER_APP_KEY') }}', {
            cluster: '{{ env('PUSHER_APP_CLUSTER') }}',
            encrypted: true
        });

        let channel = pusher.subscribe('sent-notification');
        channel.bind('App\\Events\\Notify', function(data) {
            let dataHtml = "";
            data.map(function(item, index) {
                dataHtml += `
                    <tr>
                        <td>${++index} </td>
                        <td>${item.name}</td>
                        <td>${item.email}</td>
                        <td>${item.phone}</td>
                        <td>${item.company}</td>
                        <td>${item.message}</td>
                        <td>${moment(item.created_at).fromNow()}</td>
                        <td></td>
                    </tr>

                `;
            });
            $('#contact-mail').html(dataHtml);

            console.log(data);
        });
    </script>
</body>

</html>
