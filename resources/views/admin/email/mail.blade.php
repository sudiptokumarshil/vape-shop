<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="images/favicon.png" rel="icon" />
    <title>Vape Fish Dubai</title>
    <meta name="author" content="harnishdesign.net">

    <!-- Web Fonts
======================= -->
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900'
        type='text/css'>

    <!-- Stylesheet
======================= -->
    <!-- <link rel="stylesheet" type="text/css" href="bootstrap.min.css" /> -->
    <!-- <link rel="stylesheet" type="text/css" href="all.min.css" /> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/') }}email/stylesheet.css" />
</head>

<body>
    <!-- Container -->
    <div class="container-fluid invoice-container">
        <!-- Header -->
        <header>
            <div class="row align-items-center">
                <div class="col-sm-7 text-center text-sm-start mb-3 mb-sm-0">
                    <img id="logo" src="{{ asset('/') }}client/assets/images/logo.png" title="Vape Fish Dubai"
                        alt="Vape Fish Dubai" width="100px" height="auto" />
                </div>
                <div class="col-sm-5 text-center text-sm-end">
                    <h4 class="text-7 mb-0">Invoice</h4>
                </div>
            </div>
            <hr>
        </header>

        <!-- Main Content -->
        <main>
            <div class="row">
                <div class="col-sm-6"><strong>Date:</strong>
                    {{ formatedDateForDisplay($details['order']['created_at']) }}</div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-6">
                    {{-- <strong>Billing Address:</strong>
                    <address>
                        Smith Rhodes<br />
                        15 Hodges Mews, High Wycombe<br />
                        HP12 3JL<br />
                        United Kingdom
                    </address> --}}
                </div>
                <div class="col-sm-6" style="text-align: right;"> <strong>Shipping Address:</strong>
                    <address>
                        {{ $details['order']['address'] }}
                    </address>
                </div>
            </div>

            <div class="card">
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead class="card-header">
                                <tr>
                                    <td class="col-7"><strong>Product Name</strong></td>
                                    <td class="col-2 text-center"><strong>Price</strong></td>
                                    <td class="col-1 text-center"><strong>Quantity</strong></td>
                                    <td class="col-2 text-end"><strong>Toal Amount</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($details['order']['invoiceDetails'] as $inv)
                                    <tr>
                                        <td class="col-7">
                                            {{ $inv->get_product ? $inv->get_product->name : '' }}
                                        </td>
                                        <td class="col-2 text-center">{{ $inv->price }}</td>
                                        <td class="col-1 text-center">{{ $inv->qty }}</td>
                                        <td class="col-2 text-end">{{ $inv->sub_total }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot class="card-footer">
                                <tr>
                                    <td colspan="3" class="text-end border-bottom-0"><strong>Total:</strong></td>
                                    <td class="text-end border-bottom-0">AED {{ $details['order']['total'] }}</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </main>
        <!-- Footer -->
        <footer class="text-center mt-4">
            <h4 class=""> Congratulations on your purchase</h4>
            {{-- <div class="btn-group btn-group-sm d-print-none">
				<a href="javascript:window.print()" class="btn btn-light border text-black-50 shadow-none">Print</a>
				<a href="" class="btn btn-light border text-black-50 shadow-none">Download</a>
			</div> --}}
        </footer>
    </div>
</body>

</html>
