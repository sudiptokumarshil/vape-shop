const successAlertMessage = (message) => {
    Swal.fire({
        position: "bottom-end",
        icon: "success",
        title: message,
        showConfirmButton: false,
        timer: 1500,
    });
};
