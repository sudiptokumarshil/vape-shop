<?php

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Crypt;

function productImagePath($imageName)
{
    return asset('admin/product/' . $imageName);
}

function sliderImagePath($imageName)
{
    return asset('admin/slider/' . $imageName);
}

function getPriceAfterDiscount($price, $discount)
{
    // $calculateDiscount = (floatval($price) * floatval($discount)) / 100;
    // $presentPrice = floatval($price) - floatval($calculateDiscount);
    $presentPrice = floatval($price) - floatval($discount);
    return floatval($presentPrice);
}

function cartNumber()
{
    return Cart::content()->count();
}

function cart()
{
    return Cart::content();
}

function total()
{
    return Cart::priceTotal();
}

function checkStockAvailable($type)
{
    $html = "";
    if ($type == 1) {
        return $html .= "<a href='#'>In Stock</a>";
    } else {
        return $html .= "<a href='#'>Out Of Stock</a>";
    }
}

function formatedDateForDisplay($date)
{
    return date('d-M-Y', strtotime($date));
}

function displayStockAvailable($type)
{
    $html = "";
    if ($type == 1) {
        return $html .= "<div class='flash'><span class='onnew'><span class='text'>new</span></span></div>";
    } else {
        return $html .= "<div class='outOfStock'><span class='onnew'><span class='text'>Out Of Stock</span></span></div>";
    }
}

function getEncrypt($value)
{
    return Crypt::encryptString($value);
}

function getDecrypt($value)
{
    return Crypt::decryptString($value);
}
