<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Traits\NotifyTrait;
use Illuminate\Http\Request;

class RealTimeBroadcastController extends Controller
{
    use NotifyTrait;
    public function sendNotification()
    {
        return self::notify("Hello sudipto");
    }
}
