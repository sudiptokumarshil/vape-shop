@extends('client.master')
@section('client_title')
Products
@endsection
@section('home')
<div class="main-content main-content-product left-sidebar">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-trail breadcrumbs">
                    <ul class="trail-items breadcrumb">

                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="content-area shop-grid-content no-banner col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="site-main">
                    <h3 class="custom_blog_title">
                        Products
                    </h3>
                    {{-- <div class="shop-top-control">
                            <form class="select-item select-form">
                                <span class="title">Sort</span>
                                <select title="sort" data-placeholder="12 Products/Page" class="chosen-select">
                                    <option value="1">12 Products/Page</option>
                                    <option value="2">9 Products/Page</option>
                                    <option value="3">10 Products/Page</option>
                                    <option value="4">8 Products/Page</option>
                                    <option value="5">6 Products/Page</option>
                                </select>
                            </form>
                            <form class="filter-choice select-form">s
                                <span class="title">Sort by</span>
                                <select title="sort-by" data-placeholder="Price: Low to High" class="chosen-select">
                                    <option value="1">Price: Low to High</option>
                                    <option value="2">Sort by popularity</option>
                                    <option value="3">Sort by average rating</option>
                                    <option value="4">Sort by newness</option>
                                    <option value="5">Sort by price: low to high</option>
                                </select>
                            </form>
                            <div class="grid-view-mode">
                                <div class="inner">
                                    <a href="listproducts.html" class="modes-mode mode-list">
                                        <span></span>
                                        <span></span>
                                    </a>
                                    <a href="gridproducts.html" class="modes-mode mode-grid  active">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </a>
                                </div>
                            </div>
                        </div> --}}
                    <ul class="row list-products auto-clear equal-container product-grid product-list" id="product-list">
                        @foreach ($products as $product)
                        <li class="product-item  col-lg-4 col-md-6 col-sm-6 col-xs-6 col-ts-12 style-1">
                            <div class="product-inner equal-element">
                                <div class="product-top">
                                    {{-- <div class="flash">
                                                <span class="onnew">
                                                    <span class="text">
                                                        new
                                                    </span>
                                                </span>
                                            </div> --}}
                                    {!! displayStockAvailable($product->is_stock) !!}
                                </div>
                                <div class="product-thumb">
                                    <div class="thumb-inner">
                                        <a href="{{ route('client.product.details', ['slug' => $product->url]) }}">
                                            <img src="{{ productImagePath($product->image) }}" alt="img">
                                        </a>
                                        <div class="thumb-group">
                                            {{-- <div class="yith-wcwl-add-to-wishlist">
                                                        <div class="yith-wcwl-add-button">
                                                            <a href="#">Add to Wishlist</a>
                                                        </div>
                                                    </div>
                                                    <a href="#" class="button quick-wiew-button">Quick View</a> --}}
                                            <div class="loop-form-add-to-cart">
                                                <button id="cartbtn-{{ $product->id }}" class="single_add_to_cart_button button" onclick="addToCart({{ $product->id }})">Add to cart
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <h5 class="product-name product_title">
                                        <a href="{{ route('client.product.details', ['slug' => $product->url]) }}">{{ $product->name }}</a>
                                    </h5>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                    <div class="pagination clearfix style3" id="paginateId">
                        {{-- <div class="nav-link">
                                {{ $products->links() }}
                    </div> --}}
                </div>
            </div>
        </div>
        <div class="sidebar col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="wrapper-sidebar shop-sidebar">
                <div class="widget woof_Widget">
                    <div class="widget widget-categories">
                        <h3 class="widgettitle">Brands</h3>
                        <ul class="list-categories">
                            @forelse ($brands as $brand)
                            <li>
                                <input type="checkbox" onclick="getCategoryProduct(this)" name="brand_id" value="{{ $brand->id }}" id="cb1-{{ $brand->id }}" />
                                <label for="cb1-{{ $brand->id }}" class="label-text">
                                    {{ $brand->name }}
                                </label>
                            </li>
                            @empty
                            <li>
                                <label style="color: red">Brands Not Found</label>
                            </li>
                            @endforelse
                        </ul>
                    </div>
                    <div class="widget widget-categories">
                        <h3 class="widgettitle">Flavours</h3>
                        <ul class="list-categories">
                            @forelse ($flavours as $flavour)
                            <li>
                                <input type="checkbox" onclick="getFlavourProduct(this)" name="flavour_id" value="{{ $flavour->id }}" id="cb1-{{ $flavour->id }}" />
                                <label for="cb1-{{ $flavour->id }}" class="label-text">
                                    {{ $flavour->name }}
                                </label>
                            </li>
                            @empty
                            <li>
                                <label style="color: red">Flavours Not Found</label>
                            </li>
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@section('front_script')
<script>
    let selectedBrandId = [];
    let selectedFlavourId = [];

    const getUnique = (array) => {
        let uniqueArray = [];
        for (i = 0; i < array.length; i++) {
            if (uniqueArray.indexOf(array[i]) === -1) {
                uniqueArray.push(array[i]);
            }
        }
        return uniqueArray;
    }

    let link = getUrl();

    const getCategoryProduct = (v) => {
        if ($(v).prop("checked")) {
            selectedBrandId.push($(v).val());
        } else {
            const index = selectedBrandId.indexOf($(v).val());
            if (index > -1) {
                selectedBrandId.splice(index, 1);
            }
        }

        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });
        $.ajax({
            datatype: "html",
            type: "GET",
            url: link + "product/get-brand-product",
            data: {
                selectedBrandId: getUnique(selectedBrandId),
            },
            success: function (data) {
                $('#product-list').html(data);
            },
        });
    }

    const getFlavourProduct = (v) => {
        if ($(v).prop("checked")) {
            selectedFlavourId.push($(v).val());
        } else {
            const index = selectedFlavourId.indexOf($(v).val());
            if (index > -1) {
                selectedFlavourId.splice(index, 1);
            }
        }
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });

        $.ajax({
            datatype: "html",
            type: "GET",
            url: link + "product/get-flavour-product",
            data: {
                selectedFlavourId: getUnique(selectedFlavourId),
                selectedBrandId: getUnique(selectedBrandId),
            },
            success: function (data) {
                $('#product-list').html(data);
            },
        });
    }

    // NOTE: THIS METHOD WILL BE USED FOR GETTING DATA BY MOUSE SCROLLING ...
    // NOTE: IT WILL TAKE TWO PARAMS 1.CATEGORY/BRAND/FLAVOUR ID 2. TYPE-> 1.CATEGORY 2.SUBCATEGORY 3.BRAND 4.FLAVOUR...
    let scrollval = 0;
    let page = 1;

    $(document).ready(function () {
        $(window).scroll(function () {
            didScroll = true;
            if ($(window).scrollTop() + $(window).height() >= $(document).height()) {
                page++;
                getDataByOnscroll("{{ request()->route('id') }}",
                    "{{ request()->route('type') }}",
                    page); //load content
            }
        });
    });


    const getDataByOnscroll = (id, type, page) => {
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });
        $.ajax({
            datatype: "html",
            type: "POST",
            url: link + "get-product-scrolling?page=" + page,
            data: {
                id: id,
                data_type: type,
            },
            success: function (data) {
                $('#product-list').append(data);
            },
        });
    }

</script>
@endsection
