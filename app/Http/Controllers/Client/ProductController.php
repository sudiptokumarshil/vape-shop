<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductColor;
use App\Traits\CommonTrait;
use App\Traits\GetDataListTrait;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    use CommonTrait; // NOTE: FOR GETTING ....
    use GetDataListTrait;
    public function productDetails($slug)
    {
        $categories = $this->getCategory(); //NOTE: FOR GETTING CATEGORIES ...

        $product = Product::with(['get_color'])
            ->where('url', $slug)
            ->where('status', 1)
            ->first();

        $relatedProducts = Product::select('id', 'name', 'description', 'status', 'url', 'price', 'discount', 'image', 'is_stock')
            ->where('url', '<>', $slug)
            ->where('status', 1)
            ->take(12)
            ->orderBy('id', 'desc')
            ->get();

        return view('client.layouts.pages.product_details', compact('categories', 'product', 'relatedProducts'));
    }

    public function categoryProduct($id)
    {
        $products = Product::select('id', 'name', 'status', 'category_id', 'url', 'description', 'short_description', 'price', 'discount', 'image', 'is_stock')
            ->where('category_id', $id)
            ->where('status', 1)
            ->orderBy('id', 'desc')
            ->take(12)
            ->get();

        $categories = $this->getCategory(); //NOTE: FOR GETTING CATEGORIES ...
        $brands  = $this->getCategoryWiseBrand($id);
        $flavours  = $this->getCategoryWiseFlavour($id);

        return view('client.layouts.pages.category_product', [
            'products' => $products,
            'categories' => $categories,
            'brands' => $brands,
            'flavours' => $flavours,
        ]);
    }
    public function subCategoryProduct($id)
    {
        $products = Product::select('id', 'name', 'status', 'subcategory_id', 'url', 'description', 'short_description', 'price', 'discount', 'image', 'is_stock')
            ->where('subcategory_id', $id)
            ->where('status', 1)
            ->orderBy('id', 'desc')
            ->take(12)
            ->get();

        $categories = $this->getCategory(); //NOTE: FOR GETTING CATEGORIES ...
        $brands  = $this->getCategoryWiseBrand($id);
        $flavours  = $this->getCategoryWiseFlavour($id);

        return view('client.layouts.pages.category_product', [
            'products' => $products,
            'categories' => $categories,
            'brands' => $brands,
            'flavours' => $flavours,
        ]);
    }

    public function brandProduct($id)
    {
        $products = Product::select('id', 'name', 'status', 'brand_id', 'url', 'description', 'short_description', 'price', 'discount', 'image', 'is_stock')
            ->where('brand_id', $id)
            ->where('status', 1)
            ->orderBy('id', 'desc')
            ->take(12)
            ->get();

        $gettingBrand = Category::select('id', 'root_id')->where('id', $id)->first();
        $categories = $this->getCategory(); //NOTE: FOR GETTING CATEGORIES ...
        $brands  = $this->getCategoryWiseBrand($gettingBrand->root_id);
        $flavours  = $this->getCategoryWiseFlavour($gettingBrand->root_id);

        return view('client.layouts.pages.category_product', [
            'products' => $products,
            'categories' => $categories,
            'brands' => $brands,
            'flavours' => $flavours,
        ]);
    }

    public function flavourProduct($id)
    {
        $products = Product::select('id', 'name', 'status', 'flavour_id', 'url', 'description', 'short_description', 'price', 'discount', 'image', 'is_stock')
            ->where('flavour_id', $id)
            ->where('status', 1)
            ->orderBy('id', 'desc')
            ->take(12)
            ->get();

        $gettingBrand = Category::select('id', 'root_id')->where('id', $id)->first();
        $categories = $this->getCategory(); //NOTE: FOR GETTING CATEGORIES ...
        $brands  = $this->getCategoryWiseBrand($gettingBrand->root_id);
        $flavours  = $this->getCategoryWiseFlavour($gettingBrand->root_id);

        return view('client.layouts.pages.category_product', [
            'products' => $products,
            'categories' => $categories,
            'brands' => $brands,
            'flavours' => $flavours,
        ]);
    }

    public function getBrandProduct(Request $request)
    {
        $products = Product::select('id', 'name', 'status', 'flavour_id', 'url', 'description', 'short_description', 'price', 'discount', 'image', 'is_stock')
            ->whereIn('brand_id', !empty($request->selectedBrandId[0]) ? $request->selectedBrandId : [])
            ->where('status', 1)
            ->take(50)
            ->get();
        $dataHtml = "";
        foreach ($products as $product) {
            $dataHtml .= "<li class='product-item  col-lg-4 col-md-6 col-sm-6 col-xs-6 col-ts-12 style-1'>" .
                "<div class='product-inner equal-element'>" .
                " <div class='product-top'>" . displayStockAvailable($product->is_stock) . "</div>" .
                "<div class='product-thumb'>" .
                "<div class='thumb-inner'>" .
                "<a href='" . route('client.product.details', ['slug' => $product->url]) . "'>" .
                "<img src='" . productImagePath($product->image) . "' alt='img'>" .
                "</a>" .
                "<div class='thumb-group'>" .
                "<div class='loop-form-add-to-cart'>" .
                "<button id='cartbtn-.$product->id.'
                                            class='single_add_to_cart_button button'
                                            onclick='addToCart(" . $product->id . ")'>Add to cart
                                        </button>" .
                " </div>" .
                "</div>" .
                "</div>" .
                " </div>" .
                " <div class='product-info'>" .
                " <h5 class='product-name product_title'>" .
                "<a href='" . route('client.product.details', ['slug' => $product->url]) . "'>$product->name</a>" .
                "</h5>" .
                "</div>" .
                " </div>" .
                "</li>";
        }

        return $dataHtml;
    }

    public function getFlavourProduct(Request $request)
    {
        $products = Product::select('id', 'name', 'status', 'flavour_id', 'url', 'description', 'short_description', 'price', 'discount', 'image', 'is_stock')
            ->whereIn('brand_id', !empty($request->selectedBrandId[0]) ? $request->selectedBrandId : [])
            ->whereIn('flavour_id', !empty($request->selectedFlavourId[0]) ? $request->selectedFlavourId : [])
            ->where('status', 1)
            ->take(50)
            ->get();
        $dataHtml = "";
        foreach ($products as $product) {
            $dataHtml .= "<li class='product-item  col-lg-4 col-md-6 col-sm-6 col-xs-6 col-ts-12 style-1'>" .
                "<div class='product-inner equal-element'>" .
                " <div class='product-top'>" . displayStockAvailable($product->is_stock) . "</div>" .
                "<div class='product-thumb'>" .
                "<div class='thumb-inner'>" .
                "<a href='" . route('client.product.details', ['slug' => $product->url]) . "'>" .
                "<img src='" . productImagePath($product->image) . "' alt='img'>" .
                "</a>" .
                "<div class='thumb-group'>" .
                "<div class='loop-form-add-to-cart'>" .
                "<button id='cartbtn-.$product->id.'
                                            class='single_add_to_cart_button button'
                                            onclick='addToCart(" . $product->id . ")'>Add to cart
                                        </button>" .
                " </div>" .
                "</div>" .
                "</div>" .
                " </div>" .
                " <div class='product-info'>" .
                " <h5 class='product-name product_title'>" .
                "<a href='" . route('client.product.details', ['slug' => $product->url]) . "'>$product->name</a>" .
                "</h5>" .
                "</div>" .
                " </div>" .
                "</li>";
        }

        return $dataHtml;
    }


    public function getNewArraivalProduct(Request $request)
    {
        $products = Product::select('id', 'name', 'status', 'brand_id', 'url', 'description', 'short_description', 'price', 'discount', 'image', 'is_stock')
            ->orderBy('id', 'desc')
            ->where('status', 1)
            ->paginate(12);

        $categories = $this->getCategory(); //NOTE: FOR GETTING CATEGORIES ...
        $brands  = $this->getBrand();
        $flavours  = $this->getFlavour();

        return view('client.layouts.pages.category_product', [
            'products' => $products,
            'categories' => $categories,
            'brands' => $brands,
            'flavours' => $flavours,
        ]);
    }

    public function getProductByScrolling(Request $request)
    {
        $products = Product::where(function ($q) use ($request) {
            if ($request->data_type == 1) //CATEGORY ...
                $q->where('category_id', $request->id);
            if ($request->data_type == 2) //SUBCATEGORY ...
                $q->where('subcategory_id', $request->id);
            if ($request->data_type == 3) //BRAND ...
                $q->where('brand_id', $request->id);
            if ($request->data_type == 4) //FLAVOUR ...
                $q->where('flavour_id', $request->id);
        })
            ->where('status', 1)
            ->paginate(12);
        $products->appends(['id' => $request->id]);
        $products->appends(['data_type' => $request->data_type]);

        $dataHtml = "";
        foreach ($products as $product) {
            $dataHtml .= "<li class='product-item  col-lg-4 col-md-6 col-sm-6 col-xs-6 col-ts-12 style-1'>" .
                "<div class='product-inner equal-element'>" .
                " <div class='product-top'>" . displayStockAvailable($product->is_stock) . "</div>" .
                "<div class='product-thumb'>" .
                "<div class='thumb-inner'>" .
                "<a href='" . route('client.product.details', ['slug' => $product->url]) . "'>" .
                "<img src='" . productImagePath($product->image) . "' alt='img'>" .
                "</a>" .
                "<div class='thumb-group'>" .
                "<div class='loop-form-add-to-cart'>" .
                "<button id='cartbtn-.$product->id.'
                                        class='single_add_to_cart_button button'
                                        onclick='addToCart(" . $product->id . ")'>Add to cart
                                    </button>" .
                " </div>" .
                "</div>" .
                "</div>" .
                " </div>" .
                " <div class='product-info'>" .
                " <h5 class='product-name product_title'>" .
                "<a href='" . route('client.product.details', ['slug' => $product->url]) . "'>$product->name</a>" .
                "</h5>" .
                "</div>" .
                " </div>" .
                "</li>";
        }

        return $dataHtml;
    }

    public function getColorImage($id)
    {
        $colorImage = ProductColor::select('id','color_image')->where('id',$id)->first();
        return response()->json($colorImage);
    }
}
