@extends('admin.master')
@section('titles')
    Product
@endsection
@section('admin_home')
    <style>
        html * {
            box-sizing: border-box;
        }

        p {
            margin: 0;
        }

        .upload {
            &__box {
                padding: 40px;
            }

            &__inputfile {
                width: .1px;
                height: .1px;
                opacity: 0;
                overflow: hidden;
                position: absolute;
                z-index: -1;
            }

            &__btn {
                display: inline-block;
                font-weight: 600;
                color: #fff;
                text-align: center;
                min-width: 116px;
                padding: 5px;
                transition: all .3s ease;
                cursor: pointer;
                border: 2px solid;
                background-color: #4045ba;
                border-color: #4045ba;
                border-radius: 10px;
                line-height: 26px;
                font-size: 14px;

                &:hover {
                    background-color: unset;
                    color: #4045ba;
                    transition: all .3s ease;
                }

                &-box {
                    margin-bottom: 10px;
                }
            }

            &__img {
                &-wrap {
                    display: flex;
                    flex-wrap: wrap;
                    margin: 0 -10px;
                }

                &-box {
                    width: 200px;
                    padding: 0 10px;
                    margin-bottom: 12px;
                }

                &-close {
                    width: 24px;
                    height: 24px;
                    border-radius: 50%;
                    background-color: rgba(0, 0, 0, 0.5);
                    position: absolute;
                    top: 10px;
                    right: 10px;
                    text-align: center;
                    line-height: 24px;
                    z-index: 1;
                    cursor: pointer;

                    &:after {
                        content: '\2716';
                        font-size: 14px;
                        color: white;
                    }
                }
            }
        }

        .img-bg {
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            position: relative;
            padding-bottom: 100%;
        }
    </style>
    <div class="white_box mb_30">
        <div class="box_header ">
            <div class="main-title">
                <h3 class="mb-0">Create Product</h3>
            </div>
        </div>
        @include('admin.layouts.message')
        <div class="row">
            <div class="col-1"></div>
            <div class="col-10">
                <form action="{{ route('admin.product.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Name</label>
                                <input type="text" required value="{{ old('name') }}" name="name"
                                    class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Category</label>
                                <select name="category_id" required onchange="getSubcategory(this.value)"
                                    class="form-control select_2">
                                    <option value="0">Choose One</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Sub Category</label>
                                <select name="subcategory_id" id="subCateId" class="form-control select_2">
                                    <option value="0">Choose One</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Brand</label>
                                <select name="brand_id" id="brandId" class="form-control select_2">
                                    <option value="0">Choose One</option>
                                </select>
                            </div>
                        </div>

                        {{-- <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Flavour</label>
                                <select name="flavour_id" id="flavourId" class="form-control select_2">
                                    <option value="0">Choose One</option>
                                </select>
                            </div>
                        </div> --}}

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Price</label>
                                <input type="number" required value="{{ old('price') }}" name="price"
                                    class="form-control" />
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Cover Image</label>
                                <input id="file-input" required name="image[]" class="form-control" type="file" multiple>
                                <div id="preview"></div>
                            </div>
                        </div>


                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Discount</label>
                                <input type="number" value="{{ old('discount') }}" name="discount" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Stock</label>
                                <input type="number" value="{{ old('stock') }}" name="stock" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Stock Available </label>
                                <select name="is_stock" required class="form-control">
                                    <option value="1">Stock Available</option>
                                    <option value="2">Out Of Stock</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Short Description</label>
                                <textarea name="short_description" id="short-description" class="form-control">{!! old('short_description') !!}</textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Long Description</label>
                                <textarea name="description" id="long-description" class="form-control">{!! old('description') !!}</textarea>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Title</label>
                                <input name="title" required id="title" value="{!! old('title') !!}"
                                    class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Meta</label>
                                <input name="meta" required id="meta" value="{!! old('meta') !!}"
                                    class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Url</label>
                                <input name="url" required id="url" value="{!! old('url') !!}"
                                    class="form-control" />
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Status</label>
                                <select name="status" class="form-control">
                                    <option value="1">Active</option>
                                    <option value="2">Inactive</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">

                        </div>

                        <div class="col-md-12">
                            <label for="exampleInputEmail1">Colors/Flavours</label>
                            <div class="form-group" id="colors">
                                <div class="row" id="color-option-0">
                                    <div class="col-md-4">
                                        <input type="text" required name="color[]" class="form-control" />
                                    </div>
                                    <div class="col-md-4">
                                        <input type="file" required name="color_image[]" class="form-control" />
                                    </div>
                                    <div class="col-md-4">
                                        <div class="btn-group">
                                            <button type="button" onclick="IncColor(0)"
                                                class="btn btn-success btn-sm">+</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <button type="submit" class="btn btn-primary btn-sm">Save</button>
                    <a href="{{ route('admin.product.list') }}" class="btn btn-info btn-sm">Go To List</a>
                </form>
            </div>
            <div class="col-1"></div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://cdn.ckeditor.com/ckeditor5/34.0.0/classic/ckeditor.js"></script>
    <script>
        // NOTE: FOR DISPLAYING SELECTED PRODUCT IMAGE ...
        $('#blah').hide();

        const readURL = (input) => {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#blah').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
            $('#blah').show();
        }
        $("#img").change(function() {
            readURL(this);
        });

        // NOTE: FOR GETTING CATEGORY WISE SUBCATEGORY ...
        const getSubcategory = (id) => {
            $.ajax({
                url: "/admin/category/get-subcategory/" + id,
                type: "GET",
                data: {
                    _token: "{{ csrf_token() }}",
                },
                success: function(data) {
                    $('#subCateId').empty();
                    $('#brandId').empty();
                    $('#flavourId').empty();
                    let dataHtml = "";
                    let dataBHtml = "";
                    let dataFHtml = "";
                    dataHtml += `<option value='0'>Choose One</option>`;
                    $.each(data.subCategory, function(index, item) {
                        dataHtml += `<option value='${item.id}'>${item.name}</option>`;
                    });
                    dataBHtml += `<option value='0'>Choose One</option>`;
                    $.each(data.brands, function(index, item) {
                        dataBHtml += `<option value='${item.id}'>${item.name}</option>`;
                    });
                    dataFHtml += `<option value='0'>Choose One</option>`;
                    $.each(data.flavours, function(index, item) {
                        dataFHtml += `<option value='${item.id}'>${item.name}</option>`;
                    });
                    $('#subCateId').html(dataHtml);
                    $('#brandId').html(dataBHtml);
                    $('#flavourId').html(dataFHtml);
                },
            });
        }
    </script>
    <script>
        ClassicEditor
            .create(document.querySelector('#short-description'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });

        ClassicEditor
            .create(document.querySelector('#long-description'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
    </script>
    <script>
        function previewImages() {

            var $preview = $('#preview').empty();
            if (this.files) $.each(this.files, readAndPreview);

            function readAndPreview(i, file) {

                if (!/\.(jpe?g|png|gif)$/i.test(file.name)) {
                    return alert(file.name + " is not an image");
                } // else...

                var reader = new FileReader();

                $(reader).on("load", function() {
                    $preview.append($("<img/>", {
                        src: this.result,
                        height: 100
                    }));
                });

                reader.readAsDataURL(file);

            }
        }

        $('#file-input').on("change", previewImages);

        const IncColor = (id) => {
            id++
            let dataHtml = '';
            dataHtml += `<br>
                    <div class="row" id="color-option-${id}">
                        <div class="col-md-4">
                            <input type="text" required name="color[]" class="form-control" />
                        </div>
                        <div class="col-md-4">
                            <input type="file" required name="color_image[]" class="form-control" />
                        </div>
                        <div class="col-md-4">
                            <div class="btn-group">
                                <button type="button" onclick="IncColor(${id})"
                                    class="btn btn-success btn-sm">+</button>
                                    &nbsp;
                                <button type="button" onclick="decColor(${id})" class="btn btn-danger btn-sm">-</button>
                            </div>
                        </div>
                    </div>
                `;
            $('#colors').append(dataHtml);

        }

        const decColor = (id) => {
            console.log(id);
            $('#color-option-' + id).remove();

        }
    </script>
@endsection
