@extends('admin.master')
@section('titles')
    Brand
@endsection
@section('admin_home')
    <div class="white_box mb_30">
        <div class="box_header ">
            <div class="main-title">
                <h3 class="mb-0">Create Brand</h3>
            </div>
        </div>
        @include('admin.layouts.message')
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <form action="{{ route('admin.brand.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select name="status" class="form-control">
                            <option value="1">Active</option>
                            <option value="2">Inactive</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm">Save</button>
                    <a href="{{ route('admin.brand.list') }}" class="btn btn-info btn-sm">Go To List</a>
                </form>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
@endsection
