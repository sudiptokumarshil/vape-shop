@extends('client.master')
@section('client_title')
Privacy Information
@endsection
@section('home')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumb-trail breadcrumbs">
                <ul class="trail-items breadcrumb">
                    <li class="trail-item trail-begin">
                        <a href="index.html">Home</a>
                    </li>
                    <li class="trail-item trail-end active">
                        Privacy Policy
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="content-area content-contact col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="site-main">
                <h3 class="custom_blog_title">Privacy Policy</h3>
            </div>
        </div>
    </div>
</div>
<div class="page-main-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="main-info-post">
                    <p>
                        We are committed to providing the highest level of privacy protection for all of our customers. This privacy policy helps you understand the range of information we might
                        collect
                        from you upon your visit to the website and its subpages. This page also clarifies how we may use and process information from the choices you make when using the site.
                    </p>
                    <h3 class="post-title  ">
                        <a href="#">Regarding the Information Collected</a>
                    </h3>
                    <p>
                        When you visit the website and its subpages, use our services, place an order, make a purchase, participate in our Loyalty Program, or contact us, we collect your identifiable
                        information, for example, your name, phone number, email address, etc. Please be informed that we also keep records of your browsing history and interests to enhance your user
                        experience on our site.
                    </p>
                    <h3 class="post-title  ">
                        <a href="#">Personal Identification Information (PII)</a>
                    </h3>
                    <p>
                        We may adopt several ways to gather PII from users. The following occasions might include: when you visit the site or platforms and perform certain activities such as filling
                        out forms, registering, using the site’s services, resources, or features we make available on it. You may be requested to share your email address. You also have the option to
                        visit the site anonymously. We might collect your PII only if you deliberately submit such kind of information. Please note that you can always avoid providing PII unless that
                        might obstruct you from engaging in certain activities on the site.
                    </p>
                    <h3 class="post-title  ">
                        <a href="#">Non-Personal Identification Information</a>
                    </h3>
                    <p>
                        We may also collect your non-personal identification information when you interact with the site or platforms. Here, the non-personal identification information may include
                        browser information, computer specification, and other related technical information as to how you connect to the site and platforms, for instance, the Internet Service
                        Provider (ISP), the Operating System (OS), or other similar information.
                    </p>
                    <h3 class="post-title  ">
                        <a href="#">How do we use the Collected Information?</a>
                    </h3>
                    <p  style="margin-bottom: 10px !important">
                        <b>Improve customer service:</b> Any information that you may provide us with helps us respond to your customer service support requests more efficiently.
                    </p>
                    <p style="margin-bottom: 10px !important">
                        <b>To run a survey, promotion, or other features of the Site:</b> The information that you send to us to receive services will help us offer you an optimized user experience.
                    </p>
                    <p>
                        <b>Send periodic emails:</b> We may use the email addresses of our users to keep them posted with updates, feeds, and other relevant information pertaining to their interests
                        and order. This may also help us come up with an even better response to their inquiries and requests. When a user registers to our mailing service, he or she might receive
                        recurring emails that could contain updates, newsletters, company news, and information about relevant services. Users on our site can always unsubscribe from email
                        subscriptions.
                    </p>
                    <h3 class="post-title  ">
                        <a href="#">Your Information and Our Promise</a>
                    </h3>
                    <p>
						We prioritize the privacy and security of customers’ personal information. We adopt some of the most secure and technologically advanced storage systems, data collection
                        methods, and processing practices and take tight security measures to protect our site and platforms against unauthorized or illegal access, disclosure, alteration, or
                        destruction of Users’ passwords, personal information, usernames, and transaction information stored on the site and platforms.
                    </p>
                    <h3 class="post-title  ">
                        <a href="#">Sharing our Users’ Personal Information</a>
                    </h3>
                    <p>
						We never sell, trade, or rent our users’ PII to any third party or other. However, we may share generic aggregated demographic information that is not linked to any PII or whatsoever about visitors or users with any of our business partners, advertisers or trusted affiliates for the purposes stated above. Please note that we may occasionally need to use third-party services to operate certain aspects of our business and administer activities from our end, such as monetary transactions, sending out newsletters, or surveys. In that case, we might have to share the site’s users’ information with these third parties to ensure certain services provided that the visitors of the site have permitted us to do so.
                    </p>
                    <h3 class="post-title  ">
                        <a href="#">Modification</a>
                    </h3>
                    <p>
						We reserve the right to modify, extend or bring any updates to this privacy policy at any time without prior notice. Upon bringing an update, we will put the updated date at the bottom of the page. Users are requested to visit this page frequently to remain informed about any changes as to how and in what manner we protect the personal information that we collect from our users. You agree and acknowledge that it falls into your responsibility to check this privacy policy frequently and remain informed about modifications.
                    </p>
                    <h3 class="post-title  ">
                        <a href="#">Your Acceptance of these Terms</a>
                    </h3>
                    <p>
						You consciously accept the Privacy Policy of the site and platforms by using it all the time. If you do not wish to give consent to any or all terms of this Privacy Policy, please stop using this website immediately. Your continued use of the site and platform, even after changes being made to this policy, will be regarded as your acceptance of these modifications.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
