<?php

namespace App\Traits;

use App\Models\Email;
use App\Models\Order;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

trait EmailTrait
{
    // SEND EMAIL FROM ADMIN PART ...
    public function sendEmail($order_id, $subject, $body)
    {
        $mailCredential = Email::orderBy('id', 'desc')->first();
        $config = array(
            'driver'     =>     'smtp',
            'host'       =>      $mailCredential->host,
            'port'       =>      $mailCredential->port,
            'username'   =>      $mailCredential->email,
            'password'   =>      $mailCredential->password,
            'encryption' =>      $mailCredential->encryption,
            'from'       =>     array('address' => $mailCredential->email, 'name' => 'Vape Fish Dubai'),
        );
        Config::set('mail', $config);

        $order = Order::with(['invoiceDetails'])->find($order_id);
        $data = [
            'email' => $order->email,
            'subject' => $subject,
            'description' => $body,
            'order' => $order,
        ];
        Mail::send(new \App\Mail\Mail($data));
    }
}
