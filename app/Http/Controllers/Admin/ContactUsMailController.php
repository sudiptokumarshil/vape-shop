<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ContactMail;
use Illuminate\Http\Request;

class ContactUsMailController extends Controller
{
    public function index()
    {
        $mails = ContactMail::orderBy('id', 'desc')->paginate(20);
        return view('admin.contact_mail.index', compact('mails'));
    }
}
