<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\ContactMail;
use App\Traits\CommonTrait;
use App\Traits\NotifyTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
    use CommonTrait;
    use NotifyTrait;

    public function index()
    {
        $categories = $this->getCategory(); //NOTE: FOR GETTING CATEGORIES ...
        return view('client.layouts.pages.contact', compact('categories'));
    }

    public function sendMessage(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'message' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $contactMail = new ContactMail();
            $contactMail->name = $request->name;
            $contactMail->email = $request->email;
            $contactMail->phone = $request->phone;
            $contactMail->company = $request->company;
            $contactMail->message = $request->message;
            $contactMail->save();
            $allMail = ContactMail::orderBy('id', 'desc')->take(10)->get();
            DB::commit();
            self::notify($allMail);
            return redirect()->back()->with('success', 'Your Message Sent Successfully!');
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }
}
