<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class FilterController extends Controller
{
    // NOTE : FOR PRODUCT FILTER ...
    public function productFilter(Request $request)
    {
        $categories = Category::select('id', 'name', 'status', 'root_id')
            ->where('root_id', null)
            ->get();

        $products = Product::with([
            'get_category:id,name',
            'get_subcategory:id,name',
            'get_brand:id,name',
            'get_color'
        ])->where(function ($q) use ($request) {
            if (!empty($request->product_name)) {
                $q->where('name', 'LIKE', "%{$request->product_name}%");
            }
            if (!empty($request->category_id)) {
                $q->where('category_id', $request->category_id);
            }
            if (!empty($request->subcategory_id)) {
                $q->where('subcategory_id', $request->subcategory_id);
            }
            if (!empty($request->brand_id)) {
                $q->where('brand_id', $request->brand_id);
            }
            if (!empty($request->status)) {
                $q->where('status', $request->status);
            }
        })
            ->orderBy('id', 'desc')
            ->paginate(50);

        $products->appends(['name' => $request->name]);
        $products->appends(['category_id' => $request->category_id]);
        $products->appends(['subcategory_id' => $request->subcategory_id]);
        $products->appends(['brand_id' => $request->brand_id]);
        $products->appends(['status' => $request->status]);

        return view('admin.product.list', [
            'products' => $products,
            'categories' => $categories
        ]);
    }
}
