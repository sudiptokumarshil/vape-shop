<?php

use App\Http\Controllers\Client\AboutUsController;
use App\Http\Controllers\Client\CartController;
use App\Http\Controllers\Client\CheckoutController;
use App\Http\Controllers\Client\HomeController;
use App\Http\Controllers\Client\ProductController;
use App\Http\Controllers\Client\PrivacyController;
use App\Http\Controllers\Client\ContactController;
use App\Http\Controllers\Client\PaymentController;
use App\Http\Controllers\Client\ShippingController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/product/details/{slug}', [ProductController::class, 'productDetails'])->name('client.product.details');
Route::get('/product/category-product/{id}/{type}', [ProductController::class, 'categoryProduct'])->name('client.product.category_product');
Route::get('/product/subcategory-product/{id}/{type}', [ProductController::class, 'subCategoryProduct'])->name('client.product.subcategory_product');
Route::get('/product/brand-product/{id}/{type}', [ProductController::class, 'brandProduct'])->name('client.product.brand_product');
Route::get('/product/flavour-product/{id}/{type}', [ProductController::class, 'flavourProduct'])->name('client.product.flavour_product');

Route::get('/product/get-brand-product', [ProductController::class, 'getBrandProduct']);
Route::get('/product/get-flavour-product', [ProductController::class, 'getFlavourProduct']);
Route::get('/product/get-new-product', [ProductController::class, 'getNewArraivalProduct'])->name('client.product.get_new_product');

// NOTE : FOR CART MANAGEMENT ...
Route::get('/product/cart/index', [CartController::class, 'index'])->name('client.product.cart.index');
Route::post('/product/cart/store', [CartController::class, 'store'])->name('client.product.cart.store');
Route::post('/product/cart/update', [CartController::class, 'update'])->name('client.product.cart.update');
Route::post('/product/cart/delete', [CartController::class, 'delete'])->name('client.product.cart.delete');

// NOTE: FOR ORDER CHECKOUT ...
Route::get('/product/checkout/index', [CheckoutController::class, 'index'])->name('client.product.checkout.index');
Route::post('/product/checkout/confirm-order', [CheckoutController::class, 'confirmOrder'])->name('client.product.checkout.confirm');

// NOTE: FOR PRODUCT SEARCHING ...
Route::post('/search', [HomeController::class, 'search'])->name('search');

// NOTE: GET PRODUCTS BY MOUSE SCROLLING ...
Route::post('/get-product-scrolling', [ProductController::class, 'getProductByScrolling'])->name('get_onscrolling_product');

// NOTE: GET COLOR WISE PRODUCT IMAGE ...
Route::get('/get-color-image/{id}', [ProductController::class, 'getColorImage'])->name('get_color_image');

//FOR CACHING ...
Route::get('/cache', function () {
    Artisan::call('optimize:clear');
    Artisan::call('config:cache');
    Artisan::call('view:cache');
    Artisan::call('route:cache');
    echo  "Done";
});

Route::get('/about-us', [AboutUsController::class, 'index'])->name('about_us');
// privacy policy routes
Route::get('/privacy', [PrivacyController::class, 'index'])->name('privacy');
// contact us routes
Route::get('/contact', [ContactController::class, 'index'])->name('contact');
Route::post('/contact', [ContactController::class, 'sendMessage'])->name('send_message');
// payment us routes
Route::get('/payment', [PaymentController::class, 'index'])->name('payment');
// shipping us routes
Route::get('/shipping', [ShippingController::class, 'index'])->name('shipping');

