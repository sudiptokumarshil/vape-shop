const addToCart = (id) => {
    let link = getUrl();
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        type: "POST",
        url: link + "product/cart/store",
        data: {
            id: id,
        },
        success: function (data) {
            $(".cart_qty").text(data.count);
            cartInfo(data);
            Swal.fire({
                position: "top-end",
                icon: "success",
                title: "Product Added Into Cart.",
                showConfirmButton: false,
                timer: 1500,
            });
        },
    });
};

const cartInfo = (data) => {
    $(".cart_qty").text(data.count);
    $.each(data.list, (index, item) => {
        let qty = item.qty;
        let unitPrice = item.price;
        let totalPrice = unitPrice * qty;
        $(".totalPrice-" + item.rowId).text(totalPrice);
    });
    $(".final_amount_before").text(data.total);
};

const increaseQty = (v) => {
    let link = getUrl();
    let rowId = $(v).attr("data-id");
    let qty = $(".qty-" + rowId).val();
    qty++;
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        type: "POST",
        url: link + "product/cart/update",
        data: {
            qty: qty,
            rowId: rowId,
            status: 1,
        },
        success: function (data) {
            $(".qty-" + rowId).val(Number(data.qty.qty));
            cartInfo(data);
        },
    });
};

const decreaseQty = (v) => {
    let link = getUrl();
    let rowId = $(v).attr("data-id");
    let qty = $(".qty-" + rowId).val();
    if (qty == 1) {
        qty = 1;
    } else {
        qty--;
    }
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        type: "POST",
        url: link + "product/cart/update",
        data: {
            qty: qty,
            rowId: rowId,
            status: 2,
        },
        success: function (data) {
            $(".qty-" + rowId).val(Number(data.qty.qty));
            cartInfo(data);
        },
    });
};

const cartRemove = (v) => {
    let link = getUrl();
    let rowId = $(v).attr("data-id");
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        type: "POST",
        url: link + "product/cart/delete",
        data: {
            rowId: rowId,
        },
        success: function (data) {
            $(".tableRow-" + rowId).remove();
            cartInfo(data);
        },
    });
};
