<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::orderBy('id', 'desc')
            ->where('root_id', null)
            ->paginate(5);
        return view('admin.category.list', [
            'categories' => $categories
        ]);
    }

    public function create()
    {
        return view('admin.category.create');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            // 'name' => 'required||unique:categories,name',
        ]);

        $category = new Category();
        $category->name = $request->name;
        $category->status = $request->status;
        $category->save();
        return redirect()->route('admin.category.list')->with('success', 'Category Saved Successfully!!');
    }

    public function edit($id)
    {
        $category = Category::find($id);
        return view('admin.category.edit', [
            'category' => $category,
        ]);
    }
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            // 'name' => 'required|unique:categories,name,' . $request->id,
        ]);

        $category = Category::find($request->id);
        $category->name = $request->name;
        $category->status = $request->status;
        $category->update();
        return redirect()->route('admin.category.list')->with('success', 'Category Updated Successfully!!');
    }
}
