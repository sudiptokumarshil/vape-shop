<?php

namespace App\Traits;

use App\Models\Category;

trait GetDataListTrait
{

    public function getCategoryWiseBrand($id)
    {
        $brand = Category::select('id', 'name', 'root_id', 'status', 'type')
            ->where('root_id', $id)
            ->where('type', 2)
            ->get();
        return $brand;
    }

    public function getCategoryWiseFlavour($id)
    {
        $flavour = Category::select('id', 'name', 'root_id', 'status', 'type')
            ->where('root_id', $id)
            ->where('type', 3)
            ->get();
        return $flavour;
    }
}
